package com.zj.marriage_network;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author lx
 */
@SpringBootApplication
public class MarriageNetworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarriageNetworkApplication.class, args);
    }

}
