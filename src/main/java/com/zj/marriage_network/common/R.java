package com.zj.marriage_network.common;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: ZJ
 * @Date: 2022/8/29 22:44
 * @Decsription: com.ambow.mybatis_plus.common
 */
@Data
public class R {

    private Integer code;

    private String message;

    private Map<String,Object> data=new HashMap<>();

    /**
     * 无参构造
     * 私有化单例
     * 目的：防止外部创建对象
     */
    public R() {
    }

    /**
     * 结果集的
     * @param code
     * @param message
     */
    public R(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    /**
     * 获取结果成功
     * @return
     */
    public static R ok(){
        R r=new R();
        r.setCode(ResponseEnum.SUCCESS.getCode());
        r.setMessage(ResponseEnum.SUCCESS.getMessage());
        return r;
    }

    /**
     * 获取结果失败
     * @return
     */
    public static R error(){
        R r=new R();
        r.setCode(ResponseEnum.ERROR.getCode());
        r.setMessage(ResponseEnum.ERROR.getMessage());
        return r;
    }

    /**
     * 设置一个特定的结果集
     * @return
     */
    public static R setResult(ResponseEnum responseEnum){
        R r=new R();
        r.setCode(responseEnum.getCode());
        r.setMessage(responseEnum.getMessage());
        return r;
    }


    /**
     * 设定个性化数据
     * @param key
     * @param value
     * @return {@link R}
     */
    public R data(String key,Object value){
        this.data.put(key,value);
        return this;
    }


    /**
     * 考虑到传值刚好是map集合时，采用以下方法
     * @param map
     * @return {@link R}
     */
    public R data(Map<String,Object> map){
        this.setData(map);
        return this;
    }

    /**
     * 设置个性化的响应信息
     * @param message
     * @return {@link R}
     */
    public R message(String message){
        this.setMessage(message);
        return this;
    }

    /**
     * 设置个性化的响应
     * @param code
     * @return {@link R}
     */
    public R code(Integer code){
        this.setCode(code);
        return this;
    }
}
