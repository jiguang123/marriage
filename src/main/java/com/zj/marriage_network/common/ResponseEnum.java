package com.zj.marriage_network.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @Author: ZJ
 * @Date: 2022/8/29 22:40
 * @Decsription: com.ambow.mybatis_plus.common
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public enum ResponseEnum {
    SUCCESS(0,"成功"),
    ERROR(-10,"服务器内部错误"),
    //公共非法数据错误码
    DATA_ERROR(-1, "非法数据！"),
    VALIDATE_ENTITY_ERROR(-2, ""),
    CPACHA_EMPTY(-3, "验证码不能为空!"),
    CPACHA_ERROR(-4, "验证码输入错误!"),
    SESSION_EXPIRED(-5, "还未登录或会话失效，请刷新页面或者重新登录！"),
    UPLOAD_PHOTO_SUFFIX_ERROR(-6, "图片格式不正确！"),
    UPLOAD_PHOTO_ERROR(-7, "图片上传错误！"),
    SYSTEM_CPACHA_EMPTY(-8, "请先点击发送邮件验证码！"),
    FOREIGN_KEY_RESTRAIN(-9, "该用户还存在外键约束，不可删除！"),

    //-1xx 服务器错误
    BAD_SQL_GRAMMAR_ERROR(-101, "sql语法错误"),
    SERVLET_ERROR(-102, "servlet请求异常"), //-2xx 参数校验
    UPLOAD_ERROR(-103, "文件上传错误"),
    EXPORT_DATA_ERROR(104, "数据导出失败"),

    //用户数据错误码
    USER_NAME_EMPTY(-1000, "用户名不能为空"),
    USER_PASSWORD_EMPTY(-1001, "密码不能为空!"),
    USER_NO_EXIST(-1002, "用户不存在!"),
    USER_PASSWORD_ERROR(-1003, "用户密码错误!"),
    USER_EDIT_ID_EMPTY(-1004, "修改的用户不存在！请联系管理员！"),
    USER_EDIT_ERROR(-1005, "修改失败！请联系管理员！"),
    USER_EMAIL_EMPTY(-1006, "邮箱地址不能为空！"),
    USER_EMAIL_TYPE_EMPTY(-1007, "邮件类别不能为空！请联系管理员！"),
    USER_EMAIL_SEND_ERROR(-1008, "邮件发送失败！请联系管理员！"),
    USER_EMAIL_TYPE_ERROR(-1009, "邮件类别获取失败！请联系管理员！"),
    USER_REGISTER_TYPE_EMPTY(-1010, "注册安博内招平台的目的不能为空！"),
    USER_REGISTER_ERROR(-1011, "注册安博内招平台的目的不能为空！"),
    USER_EMAIL_ALREADY_EXIST(-1012, "该邮箱已经注册了！"),
    USER_NAME_ALREADY_EXIST(-1013, "该用户名已经注册了！"),
    USER_EMAIL_NOT_EXIST(-1013, "该邮箱地址不存在！"),
    USER_MOBILE_EMPTY(-1014, "手机号不能为空！"),
    USER_SAVE_ERROR(-1015, "用户信息保存失败！请联系管理员"),
    USER_UPDATE_TIME_ERROR(-1016, "最后一次修改时间更新失败！请联系管理员"),
    USER_SAVE_SELF_DESCRIPTION_ERROR(-1017, "自我描述信息保存失败！请联系管理员"),
    USER_MOBILE_NOT_CORRECT(-1018, "请输入正确的手机号长度的手机号！"),
    USER_OLD_PASSWORD_NOT_CORRECT(-1019, "用户输入的旧密码不正确！"),
    USER_NEW_PASSWORD_NOT_CORRECT(-1020, "用户输入的新密码长度要在6-16之间！"),
    USER_CONFIRM_PASSWORD_NOT_CORRECT(-1021, "用户输入的确认密码和新密码不一致！"),
    USER_NEW_PASSWORD_SAVE_ERROR(-1022, "新密码信息保存失败！请联系管理员！"),
    ALIYUN_SMS_LIMIT_CONTROL_ERROR(-1023, "短信发送过于频繁"),  //业务限流
    ALIYUN_SMS_ERROR(-1024, "短信发送失败"),   //其他失败



    //职业管理和职位保存错误码
    POSITION_CATEGORY_PARENT_EMPTY(-2000, "职业类别父类不能为空，请联系管理员！"),
    POSITION_CATEGORY_ADD_ERROR(-2001, "职业类别添加失败，请联系管理员！"),
    POSITION_CATEGORY_EDIT_ID_EMPTY(-2002, "职业类别编辑id获取失败，请联系管理员！"),
    POSITION_CATEGORY_EDIT_ERROR(-2003, "职业类别编辑失败，请联系管理员！"),
    POSITION_CATEGORY_DELETE_ID_EMPTY(-2004, "职业类别删除获取id失败，请联系管理员！"),
    POSITION_CATEGORY_DELETE_ERROR(-2005, "职业类别删除失败，请联系管理员！"),
    POSITION_CATEGORY_PARENT_DELETE_ERROR(-2006, "该职业类别有子类，请先删除子类！"),
    POSITION_SAVE_ERROR(-2007, "职位保存失败，请联系管理员！"),
    POSITION_MONEY_NOT_CORRECT(-2008, "职位最低薪资不能大于最高薪资！"),
    POSITION_DELETE_ERROR(-2009, "职位删除失败！请联系管理员！"),
    POSITION_CHANGE_STATE_TO_OUT_ERROR(-2010, "职位下线失败！请联系管理员！"),
    POSITION_CHANGE_STATE_TO_WAIT_ERROR(-2010, "职位上线失败！请联系管理员！"),
    POSITION_CHANGE_STATE_ERROR(-2011, "职位状态改变失败！请联系管理员！"),


    //操作日志数据错误码
    OPERATERLOG_DELETE_ID_EMPTY(-3000, "获取删除操作日志id失败，请联系管理员！"),
    OPERATERLOG_DELETE_ERROR(-3001, "删除操作日志失败，请联系管理员！"),

    //简历管理数据错误码
    RESUME_EXPECT_WORK_ERROR(-4000, "期望工作信息保存失败，请联系管理员！"),
    RESUME_WORK_EXPERIENCE_ERROR(-4001, "工作经历信息保存失败，请联系管理员！"),
    RESUME_WORK_EXPERIENCE_COMPANY_NAME_EMPTY(-4002, "工作经历的公司名称不能为空！"),
    RESUME_WORK_EXPERIENCE_POSITION_NAME_EMPTY(-4003, "工作经历的职位名称不能为空！"),
    RESUME_WORK_EXPERIENCE_START_TIME_EMPTY(-4004, "工作经历的开始时间不能为空！"),
    RESUME_WORK_EXPERIENCE_END_TIME_EMPTY(-4005, "工作经历的结束时间不能为空！"),
    RESUME_WORK_EXPERIENCE_TIME_NOT_CORRECT(-4006, "工作经历的开始时间不能大于结束时间！"),
    RESUME_PROJECT_EXPERIENCE_START_TIME_EMPTY(-4007, "项目经验的开始时间不能为空！"),
    RESUME_PROJECT_EXPERIENCE_END_TIME_EMPTY(-4008, "项目经验的结束时间不能为空！"),
    RESUME_PROJECT_EXPERIENCE_TIME_NOT_CORRECT(-4009, "项目经验的开始时间不能大于结束时间！"),
    RESUME_PROJECT_EXPERIENCE_PROJECT_NAME_EMPTY(-4010, "项目经验的项目名称不能为空！"),
    RESUME_PROJECT_EXPERIENCE_POSITION_NAME_EMPTY(-4011, "项目经验的职务名称不能为空！"),
    RESUME_PROJECT_EXPERIENCE_ERROR(-4012, "项目经验信息保存失败，请联系管理员！"),
    RESUME_EDUCATION_BACKGROUND_START_TIME_EMPTY(-4013, "教育背景的开始年份不能为空！"),
    RESUME_EDUCATION_BACKGROUND_END_TIME_EMPTY(-4014, "教育背景的结束年份不能为空！"),
    RESUME_EDUCATION_BACKGROUND_SCHOOL_NAME_EMPTY(-4015, "教育背景的学校名称不能为空！"),
    RESUME_EDUCATION_BACKGROUND_MAJOR_EMPTY(-4016, "教育背景的专业名称不能为空！"),
    RESUME_EDUCATION_BACKGROUND_ERROR(-4017, "教育背景信息保存失败，请联系管理员！"),
    RESUME_EDUCATION_BACKGROUND_TIME_NOT_CORRECT(-4018, "教育背景的开始年份不能大于结束年份！"),
    RESUME_WORK_SHOW_SAVE_ERROR(-4019, "作品展示信息保存失败！请联系管理员！"),
    RESUME_ALREADY_SUBMIT(-4020, "该职位已经提交过简历，请换一个职位吧！"),
    RESUME_STATE_SAVE_ERROR(-4021, "简历状态信息保存失败！请联系管理员！"),
    RESUME_DELETE_ERROR(-4022, "简历删除失败！请联系管理员！"),
    RESUME_WRITE_NOT_COMPLETE(-4023, "简历填写不完整，请先完善简历！"),


    //公司管理类错误码
    COMPANY_NAME_EMPTY(-5000, "公司名称不能为空！"),
    COMPANY_VALUE_EMPTY(-5001, "公司价值观不能为空！"),
    COMPANY_NAME_WORD_OVER(-5002, "公司名称字数不能超过30个！"),
    COMPANY_VALUE_WORD_OVER(-5003, "公司价值观字数不能超过50个！"),
    COMPANY_NAME_ALREADY_EXIST(-5004, "公司名称已经存在，请换一个！"),
    COMPANY_NAME_AND_VALUE_SAVE_ERROR(-5005, "公司名称和价值观保存失败，请联系管理员！"),
    COMPANY_TAGS_SAVE_ERROR(-5006, "公司标签保存失败，请联系管理员！"),
    COMPANY_TAGS_WORD_OVER(-5007, "公司标签字数不能超过30个！"),
    COMPANY_NAME_AND_VALUE_PRIORITY(-5008, "请先填写公司名称和价值观！"),
    COMPANY_PRODUCT_NAME_WORD_OVER(-5009, "公司产品标题字数不能超过11个！"),
    COMPANY_PRODUCT_SAVE_ERROR(-5010, "公司产品信息保存失败！请联系管理！"),
    COMPANY_INTRODUCTION_SAVE_ERROR(-5011, "公司介绍信息保存失败！请联系管理！"),
    COMPANY_LOCALE_EMPTY(-5012, "公司地点不能为空！"),
    COMPANY_TERRITORY_EMPTY(-5013, "公司领域不能为空！"),
    COMPANY_URL_EMPTY(-5014, "公司网址不能为空！"),
    COMPANY_BASIC_SAVE_ERROR(-5015, "公司地点、领域、规模和网页信息保存失败！请联系管理！"),
    COMPANY_SCALE_EMPTY(-5016, "公司规模不能为空！"),
    COMPANY_FINANCE_SAVE_ERROR(-5017, "公司融资阶段信息保存失败！请联系管理员！"),
    COMPANY_FOUNDER_SAVE_ERROR(-5018, "公司创始人信息保存失败！请联系管理员！"),
    COMPANY_FOUNDER_NAME_EMPTY(-5019, "公司创始人姓名不能为空！"),
    COMPANY_FOUNDER_POSITION_EMPTY(-5020, "公司创始人职位不能为空！"),
    COMPANY_FOUNDER_NAME_WORD_OVER(-5021, "公司创始人姓名字数不能大于10个！"),
    COMPANY_FOUNDER_POSITION_WORD_OVER(-5022, "公司创始人职位字数不能大于15个！"),
    COMPANY_APPLY_SAVE_ERROR(-5023, "公司认证信息保存失败，请联系管理员！"),
    COMPANY_CHANGE_STATE_ERROR(-5024, "公司状态改变失败！请联系管理员！")
    ;


    //响应的状态值
    private Integer code;
    //响应的信息
    private String message;
}
