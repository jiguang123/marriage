package com.zj.marriage_network.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: ZJ
 * @Date: 2022/8/30 14:13
 * @Decsription: com.ambow.mybatis_plus.config
 */
@Configuration
@MapperScan("com.zj.marriage_network.mapper")
public class MybatisPlusConfig {

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(){
        //1、创建拦截器interceptor对象
        MybatisPlusInterceptor interceptor=new MybatisPlusInterceptor();

        //2、利用interceptor对象添加内部拦截器（创建 分页内部拦截器【指定mysql的方言】）
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        //3、乐观锁
        interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());

        return interceptor;
    }
}
