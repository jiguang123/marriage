package com.zj.marriage_network.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * @author lx
 * @date 2022/09/05
 */
@Configuration
@EnableSwagger2 /**启用swagger2*/
public class Swagger2Config {
    /*@Bean
    public Docket adminApiConfig(){
        return new Docket(DocumentationType.SWAGGER_2).groupName("adminApi").select().paths(Predicates.and(PathSelectors.regex("/admin/.*"))).build().apiInfo(adminInfo());
    }
    @Bean
    public Docket frontApiConfig(){
        return new Docket(DocumentationType.SWAGGER_2).groupName("frontApi").select().paths(Predicates.and(PathSelectors.regex("/front/.*"))).build().apiInfo(adminInfo());
    }*/
    @Bean
    public ApiInfo adminInfo(){
        return new ApiInfoBuilder()
                .title("安博内招后台管理系统-API文档")
                .description("本文档描述了安博内招后台管理系统接口")
                .version("1.2")
                .contact(new Contact("zj","","3151159602@qq.com")).build();
    }
}
