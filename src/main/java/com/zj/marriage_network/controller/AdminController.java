package com.zj.marriage_network.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.fastjson.JSON;
import com.zj.marriage_network.entity.*;
import com.zj.marriage_network.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/4 15:45
 * @Decsription: com.zj.marriage_network.controller
 */
@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    AdminService adminService;
    @Autowired
    UserService userService;
    @Autowired
    ShareService shareService;
    @Autowired
    ActivityService activityService;
    @Autowired
    GuidanceService guidanceService;
    @Autowired
    FeedbackService feedbackService;
    @Autowired
    LinkService linkService;

    /**
     * 登录页面
     *
     * @return {@link String}
     */
    @RequestMapping("/loginPage")
    public String loginPage(HttpSession session){
        session.removeAttribute("admin");
        return "admin/login";
    }

    /**
     * 登录
     *
     * @return {@link String}
     */
    @RequestMapping("/login")
    @ResponseBody
    public String login(String adminName, String adminPassword, HttpSession session){
        Admin admin = adminService.findAdminByNameAndPassword(adminName, adminPassword);
        if (admin == null){
            return "0";
        }else {
            session.setAttribute("admin", admin);
            return "1";
        }
    }

    /*忘记密码*/

    /**
     * 忘记密码页面
     *
     * @return {@link String}
     */
    @RequestMapping("/toForgotPasswordPage")
    public String toForgotPasswordPage(){
        return "admin/forgotPassword/forgotPassword";
    }

    /**
     * 找到管理名字和邮件
     *
     * @param adminName 管理员名称
     * @param adminMail 管理邮件
     * @return {@link String}
     */
    @RequestMapping("/findAdminByNameAndMail")
    @ResponseBody
    public String findAdminByNameAndMail(String adminName, String adminMail,HttpSession session) {
        Admin admin=adminService.findAdminByNameAndMail(adminName, adminMail);
        session.setAttribute("admin", admin);
        if(admin==null) {
            return "0";
        }else {
            return "1";
        }
    }

    /**
     * 测试和验证
     *
     * @param request 请求
     * @return {@link String}
     */
    @RequestMapping("/testAndVerify")
    @ResponseBody
    public String testAndVerify(HttpServletRequest request,Admin admin){
        HttpSession session=request.getSession();
        Admin admin1= (Admin) session.getAttribute("admin");
        if (admin1==null){
            admin1=adminService.findAdminByNameAndMail(admin.getAdminName(),admin.getAdminMail());
        }
        if (admin.getAdminQuestion1().equals(admin1.getAdminQuestion1())&&
                admin.getAdminQuestion2().equals(admin1.getAdminQuestion2())&&
                admin.getAdminQuestion3().equals(admin1.getAdminQuestion3())) {
            return "1";
        }else {
            return "0";
        }
    }

    /**
     * 更新管理密码通过id
     *
     * @param request 请求
     * @param admin   管理
     * @return {@link String}
     */
    @RequestMapping("/updateAdminPasswordById")
    @ResponseBody
    public String updateAdminPasswordById(HttpServletRequest request,Admin admin){
        HttpSession session=request.getSession();
        Admin admin1= (Admin) session.getAttribute("admin");
        admin1.setAdminPassword(admin.getAdminPassword());
        int res=adminService.updateAdminPasswordById(admin1);
        if (res >= 1){
            return "1";
        }else{
            return "0";
        }
    }

    /**
     * 管理员首页
     *
     * @return {@link String}
     */
    @RequestMapping("/adminIndex")
    public String adminIndex(){
        return "admin/index";
    }
    /**
     * 欢迎页面
     *
     * @param model 模型
     * @return {@link String}
     */
    @RequestMapping("/welcome")
    public String welcome(Model model){
        Numbers numbers=new Numbers();
        numbers.setUserNumber(userService.allUsersCount());
        numbers.setShareNumber(shareService.allCount());
        numbers.setActivityNumber(activityService.allCount());
        numbers.setGuidanceNumber(guidanceService.allCount());
        numbers.setFeedbackNumber(feedbackService.allCount());
        numbers.setLinkNumber(linkService.allCount());
        model.addAttribute("numbers", numbers);

        return "admin/welcome";
    }

    /**
     * 得到管理信息
     *
     * @return {@link String}
     */
    @RequestMapping("/adminInfo")
    public String getAdminInfo() {
        return "admin/admin-edit";
    }

    /**
     * 更新管理信息
     *
     * @param admin 管理
     * @return {@link String}
     */
    @RequestMapping("/updateAdminInfo")
    @ResponseBody
    public String updateAdminInfo(Admin admin) {
        int res=adminService.updateAdminById(admin);
        if (res >= 1) {
            return "1";
        }else {
            return "0";
        }
    }

    /*用户管理模块*/

    /**
     * 找到所有用户
     *
     * @param model 模型
     * @return {@link String}
     */
    @RequestMapping("/findAllUsers")
    public String findAllUsers(Model model){
        List<UserInfo> users = userService.findAllUsers();
        model.addAttribute("users", users);
        return "admin/user-list";
    }


    /**
     * 更新用户状态
     *
     * @param user  用户
     * @return {@link String}
     */
    @RequestMapping("/updateUserStatus")
    @ResponseBody
    public String updateUserStatus(UserInfo user){
        int res=userService.updateUserStatus(user);
        if (res >= 1){
            return "1";
        }else {
            return "0";
        }
    }

    /**
     * 错误页面
     *
     * @return {@link String}
     */
    @RequestMapping("/toErrorPage")
    public String toErrorPage(){
        return "admin/error";
    }

    /**
     * 找到用户名字
     *
     * @param model 模型
     * @param name  名字
     * @return {@link String}
     */
    @RequestMapping("/findUsersByName")
    public String findUsersByName(Model model,String name){
        List<UserInfo> users = userService.findUsersByName(name);
        model.addAttribute("users", users);
        return "admin/user-list";
    }



    /*分享管理模块*/

    /**
     * 找到所有分享
     *
     * @param model 模型
     * @return {@link String}
     */
    @RequestMapping("/findAllShare")
    public String findAllShares(Model model){
        List<Share> shareList= shareService.findAllShare();
        model.addAttribute("shareList", shareList);
        return "admin/share/share-list";
    }

    /**
     * 到分享管理的首页
     *
     * @param model 模型
     * @return {@link String}
     */
    @RequestMapping("/findAllShareOne")
    public String findAllSharesOne(Model model){
        List<Share> shareList= shareService.findAllShareByStatus(0);
        model.addAttribute("shareList", shareList);
        return "admin/share/share-list";
    }
    /**
     * 传值
     *
     * @param share   分享
     * @param session 会话
     */
    @RequestMapping("/setShare")
    @ResponseBody
    public String setShareId(Share share,HttpSession session){
        session.setAttribute("share", share);
        return "null";
    }

    /**
     * 通过状态找到分享
     *
     * @param model   模型
     * @param request 请求
     * @return {@link String}
     */
    @RequestMapping("/findAllShareByStatus")
    public String findAllShareByStatus(Model model,HttpServletRequest request){
        HttpSession session = request.getSession();
        Share share = (Share) session.getAttribute("share");
        List<Share> shareList= shareService.findAllShareByStatus(share.getShareStatus());
        model.addAttribute("shareList", shareList);
        session.removeAttribute("share");
        return "admin/share/share-list";
    }

    /**
     * 查看分享详情
     *
     * @param model   模型
     * @param request 请求
     * @return {@link String}
     */
    @RequestMapping("/lookShare")
    public String lookShare(Model model,HttpServletRequest request) {
        HttpSession session = request.getSession();
        Share share = (Share) session.getAttribute("share");
        Share share1=shareService.findShareByShareId(share.getShareId());
        model.addAttribute("share", share1);
        session.removeAttribute("share");
        return "admin/share/share-edit";
    }

    /**
     * 更新共享状态
     *
     * @param share 分享
     * @return {@link String}
     */
    @RequestMapping("/updateShareStatus")
    @ResponseBody
    public String updateShareStatus(Share share) {
        int res=shareService.updateShareStatus(share);
        if (res >= 1) {
            return "1";
        }else{
            return "0";
        }
    }

    /*活动管理模块*/

    /**
     * 找到所有活动
     *
     * @param model 模型
     * @return {@link String}
     */
    @RequestMapping("/findAllActivity")
    public String findAllActivity(Model model){
        List<Activity> activityList= activityService.findAllActivity();
        model.addAttribute("activityList", activityList);
        return "admin/activity/list";
    }

    /**
     * 传值
     *
     * @param session  会话
     * @param activity 活动
     * @return {@link String}
     */
    @RequestMapping("/setActivity")
    @ResponseBody
    public String setActivity(HttpSession session, Activity activity,Integer status){
        session.setAttribute("activity", activity);
        session.setAttribute("status", status);
        return "null";
    }

    /**
     * 找到所有活动状态
     *
     * @param model   模型
     * @param request 请求
     * @return {@link String}
     */
    @RequestMapping("/findAllActivityByStatus")
    public String findAllActivityByStatus(Model model,HttpServletRequest request){
        HttpSession session = request.getSession();
        Integer status = (Integer) session.getAttribute("status");
        List<Activity> activityList= activityService.findActivityByStatus(status);
        model.addAttribute("activityList", activityList);
        return "admin/activity/list";
    }

    /**
     * 添加活动页面
     *
     * @return {@link String}
     */
    @RequestMapping("/addActivityPage")
    public String addActivityPage(){
        return "admin/activity/add";
    }

    /**
     * 添加活动
     *
     * @param activity 活动
     * @return {@link String}
     */
    @RequestMapping("/addActivity")
    @ResponseBody
    public String addActivity(Activity activity){
        int res=activityService.addActivity(activity);
        if(res >= 1){
            return "1";
        }else {
            return "0";
        }
    }

    /**
     * 更新活动状态
     *
     * @param activity 活动
     * @return {@link String}
     */
    @RequestMapping("/updateActivityStatus")
    @ResponseBody
    public String updateActivityStatus(Activity activity){
        int res=activityService.updateActivityStatus(activity);
        if(res >= 1){
            return "1";
        }else {
            return "0";
        }
    }


    /**
     * 更新活动页面
     *
     * @return {@link String}
     */
    @RequestMapping("/updateActivityPage")
    public String updateActivityPage(HttpServletRequest request,Model model){
        HttpSession session = request.getSession();
        Activity activity = (Activity) session.getAttribute("activity");
        activity=activityService.findActivityById(activity.getActivityId());
        model.addAttribute("activity",activity);
        return "admin/activity/edit";
    }


    /**
     * 更新活动
     *
     * @param activity 活动
     * @return {@link String}
     */
    @RequestMapping("/updateActivity")
    @ResponseBody
    public String updateActivity(Activity activity){
        int res=activityService.updateActivity(activity);
        if(res >= 1){
            return "1";
        }else {
            return "0";
        }
    }

    /**
     * 查看详情
     * @param request 请求
     * @param model   模型
     * @return {@link String}
     */
    @RequestMapping("/lookInfo")
    public String lookInfo(HttpServletRequest request,Model model){
        HttpSession session = request.getSession();
        Activity activity = (Activity) session.getAttribute("activity");
        activity=activityService.findActivityById(activity.getActivityId());
        model.addAttribute("activity",activity);
        return "admin/activity/lookInfo";
    }

    @Autowired
    SignService signService;

    /**
     * 报名名单
     *
     * @param request 请求
     * @param model   模型
     * @return {@link String}
     */
    @RequestMapping("/singList")
    public String singList(HttpServletRequest request,Model model){
        HttpSession session = request.getSession();
        Activity activity = (Activity) session.getAttribute("activity");
        List<Sign> signList=signService.findAllByActivityId(activity.getActivityId());
        session.setAttribute("activityId",activity.getActivityId());
        model.addAttribute("signList",signList);
        Integer num=signService.signNumber(activity.getActivityId());
        model.addAttribute("num",num);
        return "admin/activity/singList";
    }

    /**
     * 导出活动报名名单
     *
     * @param request 请求
     * @param model   模型
     * @return {@link String}
     */
    @RequestMapping("/signExcel")
    @ResponseBody
    public String goToFirm(HttpServletRequest request,Model model){
        HttpSession session = request.getSession();
        Integer activityId = (Integer) session.getAttribute("activityId");
        List<ExportList> exportList=signService.findAllByActivityIdExcelByExcel(activityId);
        Activity activity= activityService.findActivityById(activityId);
        String filename="C:\\Users\\lx\\Desktop\\Excel"+activity.getActivityTitle()+"报名信息表.xlsx";
        EasyExcel.write(filename, ExportList.class).sheet(activity.getActivityTitle()+"报名信息表.xlsx").doWrite(exportList);
        return "1";
    }

    /*恋爱指导模块*/

    /**
     * 找到所有爱指南
     *
     * @param model 模型
     * @return {@link String}
     */
    @RequestMapping("/findAllLoveGuides")
    public String findAllLoveGuides(Model model) {
        List<Guidance> guidanceList = guidanceService.findAllGuidance();
        model.addAttribute("guidanceList", guidanceList);
        return "admin/guidance/guidance-list";
    }

    /**
     * 找到所有爱情指南
     *模糊查询
     * @param model 模型
     * @return {@link String}
     */
    @RequestMapping("/findAllLoveGuidesByLike")
    public String findAllLoveGuidesByLike(Model model,Guidance guidance) {
        List<Guidance> guidanceList = guidanceService.findAllGuidanceByLike(guidance);
        model.addAttribute("guidanceList", guidanceList);
        return "admin/guidance/guidance-list";
    }

    /**
     * 删除指导
     *
     * @param guidanceId 指导id
     * @return {@link String}
     */
    @RequestMapping("/deleteGuidance")
    @ResponseBody
    public String deleteGuidance(Integer guidanceId) {
        int res=guidanceService.deleteGuidance(guidanceId);
        if (res >= 1){
            return "1";
        }else {
            return "0";
        }
    }

    /**
     * 去添加页面
     *
     * @return {@link String}
     */
    @RequestMapping("/addGuidesPages")
    public String addGuidesPages(){
        return "admin/guidance/guidance-add";
    }

    /**
     * 添加指南
     *
     * @return {@link String}
     */
    @RequestMapping("/addGuides")
    @ResponseBody
    public String addGuides(Guidance guidance){
        java.sql.Date date=new java.sql.Date(new java.util.Date().getTime());
        guidance.setGuidanceTime(date);
        int res=guidanceService.addGuidance(guidance);
        if (res >= 1){
            return "1";
        }else {
            return "0";
        }
    }

    /**
     * 更新指南页面
     * 去修改指南的页面
     *
     * @param guidanceId 指导id
     * @param session    会话
     * @return {@link String}
     */
    @RequestMapping("/updateGuidesPages")
    public String updateGuidesPages(Integer guidanceId,HttpSession session){
        session.setAttribute("guidanceId",guidanceId);
        return "admin/guidance/guidance-edit";
    }

    /**
     * 更新指南页面
     *
     * @param model   模型
     * @param request 请求
     * @param session 会话
     * @return {@link String}
     */
    @RequestMapping("/toUpdateGuidesPages")
    public String toUpdateGuidesPages(Model model, HttpServletRequest request,HttpSession session) {
        session = request.getSession();
        Integer guidanceId = (Integer) session.getAttribute("guidanceId");
        Guidance guidance= guidanceService.getGuidanceById(guidanceId);
        session.setAttribute("guidance", guidance);
        model.addAttribute("guidance", guidance);
        return "admin/guidance/guidance-edit";
    }

    /**
     * 更新指南
     *
     * @param guidance 指导
     * @return {@link String}
     */
    @RequestMapping("/updateGuides")
    @ResponseBody
    public String updateGuides(Guidance guidance){
        int res=guidanceService.updateGuidance(guidance);
        if (res >= 1){
            return "1";
        }else {
            return "0";
        }
    }

    /*反馈管理模块*/

    /**
     * 找到所有客户反馈
     *
     * @param model 模型
     * @return {@link String}
     */
    @RequestMapping("/findAllCustomerFeedback")
    public String findAllCustomerFeedback(Model model) {
        List<Feedback> feedbackList = feedbackService.getFeedbackList();
        model.addAttribute("feedbackList", feedbackList);
        return "admin/feedback/feedback-list";
    }

    /**
     * 找到已读用户反馈
     *
     * @param model 模型
     * @return {@link String}
     */
    @RequestMapping("/findHaveReadCustomerFeedback")
    public String findHaveReadCustomerFeedback(Model model) {
        List<Feedback> feedbackList = feedbackService.findFeedbackByStatus(1);
        model.addAttribute("feedbackList", feedbackList);
        return "admin/feedback/feedback-list";
    }

    /**
     * 找到未读用户反馈
     *
     * @param model 模型
     * @return {@link String}
     */
    @RequestMapping("/findNoReadCustomerFeedback")
    public String findHaveNoCustomerFeedback(Model model) {
        List<Feedback> feedbackList = feedbackService.findFeedbackByStatus(0);
        model.addAttribute("feedbackList", feedbackList);
        return "admin/feedback/feedback-list";
    }
    /**
     * 修改反馈信息的状态
     *
     * @param model    模型
     * @param feedback feedback
     * @return {@link String}
     */
    @RequestMapping("/updateFeedbackStatus")
    @ResponseBody
    public String updateFeedbackStatus(Model model,Feedback feedback,HttpSession session) {
        session.setAttribute("feedback", feedback);
        int res=feedbackService.updateFeedbackStatus(feedback);
        if (res >= 1){
            return "1";
        }else {
            return "0";
        }
    }

    /**
     * 看反馈详情
     *
     * @param model   模型
     * @param request 请求
     * @return {@link String}
     */
    @RequestMapping("/lookFeedback")
    public String lookFeedback(Model model,HttpServletRequest request){
        HttpSession session=request.getSession();
        Feedback feedback= (Feedback) session.getAttribute("feedback");
        Feedback feed= feedbackService.findByFeedbackId(feedback.getFeedbackId());
        model.addAttribute("feedback",feed);
        return "admin/feedback/feedback-edit";
    }


    /*友情链接模块*/

    /**
     * 找到所有链接
     *
     * @param model 模型
     * @return {@link String}
     */
    @RequestMapping("/findAllLinks")
    public String findAllLinks(Model model){
        List<Link> linkList= linkService.findAllLinks();
        model.addAttribute("linkList", linkList);
        return "admin/link/link-list";
    }

    /**
     * 发现链接名字
     *
     * @param model 模型
     * @param name  名字
     * @return {@link String}
     */
    @RequestMapping("/findLinksByName")
    public String findLinksByName(Model model,String name){
        List<Link> linkList= linkService.findLinksByName(name);
        model.addAttribute("linkList", linkList);
        return "admin/link/link-list";
    }

    /**
     * 添加链接页面
     *
     * @return {@link String}
     */
    @RequestMapping("/toAddLinkPage")
    public String toAddLinkPage(){
        return "admin/link/add";
    }

    /**
     * 添加链接
     *
     * @return {@link String}
     */
    @RequestMapping("/toAddLink")
    @ResponseBody
    public String toAddLink(Link link){
        int res=linkService.addLink(link);
        if (res >= 1){
            return "1";
        }else{
            return "0";
        }
    }

    /**
     * 删除链接
     *
     * @param link 链接
     * @return {@link String}
     */
    @RequestMapping("/deleteLink")
    @ResponseBody
    public String deleteLink(Link link){
        int res=linkService.deleteLink(link);
        if (res >= 1){
            return "1";
        }else{
            return "0";
        }
    }

    /**
     * 传值
     *
     * @param session 会话
     * @param link    链接
     * @return {@link String}
     */
    @RequestMapping("/setLinkId")
    @ResponseBody
    public String setLinkId(Link link,HttpSession session){
        session.setAttribute("link", link);
        return "null";
    }

    /**
     * 更新链接页面
     *
     * @return {@link String}
     */
    @RequestMapping("/toUpdateLinkPage")
    public String toUpdateLinkPage(HttpServletRequest request,Model model){
        HttpSession session = request.getSession();
        Link link = (Link) session.getAttribute("link");
        model.addAttribute("link", linkService.findLinkByLinkId(link.getLinkId()));
        return "admin/link/edit";
    }

    /**
     * 更新链接
     *
     * @param link 链接
     * @return {@link String}
     */
    @RequestMapping("/updateLink")
    @ResponseBody
    public String updateLink(Link link){
        int res=linkService.updateLink(link);
        if (res >= 1){
            return "1";
        }else{
            return "0";
        }
    }

}
