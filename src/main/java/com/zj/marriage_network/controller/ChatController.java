package com.zj.marriage_network.controller;

import com.alibaba.fastjson.JSON;
import com.zj.marriage_network.entity.Friend;
import com.zj.marriage_network.entity.SayHello;
import com.zj.marriage_network.entity.UserInfo;
import com.zj.marriage_network.service.FriendsService;
import com.zj.marriage_network.service.SayHelloService;
import com.zj.marriage_network.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/10 11:16
 * @Decsription: com.zj.marriage_network.controller
 */
@Controller
@RequestMapping("/chat")
public class ChatController {
    @Autowired
    UserService userService;
    @Autowired
    FriendsService friendsService;
    @Autowired
    SayHelloService sayHelloService;

    /**
     * 找到用户id和其他id
     *
     * @param request 请求
     * @param userId  用户id
     * @return {@link String}
     */
    @RequestMapping("/findByUserIdAndOtherId")
    @ResponseBody
    public String findByUserIdAndOtherId(HttpServletRequest request, @RequestBody Integer userId){
        HttpSession session= request.getSession();
        UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");
        Integer userIds = userInfo.getUserId();
        SayHello sayHello =sayHelloService.findByUserIdAndOtherId(userIds, userId);
        if (sayHello != null) {
            return "1";
        }else {
            return "0";
        }
    }

    /**
     * 通过用户和用户id添加打招呼
     *
     * @param request 请求
     * @param userId  用户id
     * @return {@link String}
     */
    @RequestMapping("/addSayHelloByUserAndUserId")
    @ResponseBody
    public String addSayHelloByUserAndUserId(HttpServletRequest request, @RequestBody Integer userId){
        HttpSession session= request.getSession();
        UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");
        Integer userIds = userInfo.getUserId();
        int res=sayHelloService.addSayHello(userIds, userId);
        if (res >= 1) {
            return "1";
        }else {
            return "0";
        }
    }

    /**
     * 去聊天页面
     *
     * @return {@link String}
     */
    @RequestMapping("/toChatPage")
    public ModelAndView toChatPage(Integer userId){
        ModelAndView model=new ModelAndView();
        model.addObject("userId",userId);
        model.setViewName("/users/chat/toChatPage");
        return model;
    }


    /**
     * 去“打招呼”页面
     *
     * @param request 请求
     * @return {@link ModelAndView}
     */
    @RequestMapping("/toSayHelloPage")
    public ModelAndView toSayHelloPage(HttpServletRequest request){
        HttpSession session=request.getSession();
        UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");
        ModelAndView model=new ModelAndView();
        model.addObject("userId",userInfo.getUserId());
        model.setViewName("/users/chat/toSayHelloPage");
        return model;
    }

    /**
     * 找到我所有的打招呼
     *
     * @param userId 用户id
     * @return {@link String}
     */
    @RequestMapping("/findAllMySayHello")
    @ResponseBody
    public String findAllMySayHello(@RequestBody Integer userId){
        List<SayHello> sayHelloList = sayHelloService.findAllMySayHello(userId);
        String JsonString = JSON.toJSONString(sayHelloList);
        return JsonString;
    }

    /**
     * 删除通过id问好
     *
     * @param sayHello 说“你好”
     * @return {@link String}
     */
    @RequestMapping("/deleteSayHelloById")
    @ResponseBody
    public String deleteSayHelloById(SayHello sayHello){
        int res=sayHelloService.updateHelloStatus(sayHello);
        if (res >= 1) {
            return "1";
        }else {
            return "0";
        }
    }

    /**
     * 去用户信息页面
     *
     * @param userId 用户id
     * @return {@link ModelAndView}
     */
    @RequestMapping("/toUserInfoPageBySayHello")
    public ModelAndView toUserInfoPageBySayHello(Integer userId,String url){
        ModelAndView model=new ModelAndView();
        model.addObject("userId", userId);
        model.addObject("home", url);
        model.setViewName("/users/chat/friendInfo");
        return model;
    }

    /**
     * 我没有看过的消息的数量
     *
     * @param userId 用户id
     * @return {@link String}
     */
    @RequestMapping("/mySayHelloNoLook")
    @ResponseBody
    public String mySayHelloNoLook(@RequestBody Integer userId){
        int number= sayHelloService.mySayHelloNoLook(userId);
        String JsonString = JSON.toJSONString(number);
        return JsonString;
    }

    /**
     * 找到所有问好根据状态
     *
     * @param sayHello 说“你好”
     * @return {@link String}
     */
    @RequestMapping("/findAllMySayHelloByStatus")
    @ResponseBody
    public String findAllMySayHelloByStatus(SayHello sayHello){
        List<SayHello> sayHelloList = sayHelloService.findAllMySayHelloByStatus(sayHello);
        String JsonString = JSON.toJSONString(sayHelloList);
        return JsonString;
    }

    @RequestMapping("/updateSayHelloByMyId")
    @ResponseBody
    public String updateSayHelloByMyId(SayHello sayHello){
        int res=sayHelloService.updateHelloStatusByMy(sayHello);
        if (res >= 1) {
            return "1";
        }else {
            return "0";
        }
    }
}
