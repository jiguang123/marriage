package com.zj.marriage_network.controller;

import com.alibaba.fastjson.JSON;
import com.zj.marriage_network.entity.Favorites;
import com.zj.marriage_network.entity.Guidance;
import com.zj.marriage_network.entity.UserInfo;
import com.zj.marriage_network.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @Author: ZJ
 * @Date: 2022/9/9 16:20
 * @Decsription: com.zj.marriage_network.controller
 */
@Controller
@RequestMapping("/favorites")
public class FavoritesController {
    @Autowired
    UserService userService;
    @Autowired
    ShareService shareService;
    @Autowired
    ActivityService activityService;
    @Autowired
    GuidanceService guidanceService;
    @Autowired
    FavoritesService favoritesService;

    /**
     * 找到用户和指导
     *
     * @param request 请求
     * @return {@link String}
     */
    @RequestMapping("/findUserAndGuidance")
    @ResponseBody
    public String findUserAndGuidance(HttpServletRequest request){
        HttpSession session= request.getSession();
        Integer guidanceId= (Integer) session.getAttribute("id");
        UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");
        Favorites favorites = new Favorites();
        favorites.setFavoritesGuidance(guidanceId);
        favorites.setFavoritesUserId(userInfo.getUserId());
        session.setAttribute("favorites", favorites);
        Favorites favorites1=favoritesService.findByUserIdAndOtherId(favorites);

        if (favorites1 != null) {
            return "1";
        }else {
            return "0";
        }
    }

    /**
     * 找到指导通过id数字
     *
     * @param guidanceId 指导id
     * @return {@link String}
     */
    @RequestMapping("/findGuidanceByIdNumbers")
    @ResponseBody
    public String findGuidanceByIdNumbers(@RequestBody Integer guidanceId){
        Guidance guidance= guidanceService.getGuidanceById(guidanceId);
        int num = guidance.getGuidanceNumber();
        String JsonString = JSON.toJSONString(num);
        return JsonString;
    }

    /**
     * 添加指导收藏
     *
     * @param request 请求
     * @return {@link String}
     */
    @RequestMapping("/addGuidanceFavorites")
    @ResponseBody
    public String addGuidanceFavorites(HttpServletRequest request){
        HttpSession session= request.getSession();
        Favorites favorites = (Favorites) session.getAttribute("favorites");

        Guidance guidance=guidanceService.getGuidanceById(favorites.getFavoritesGuidance());
        Integer number=guidance.getGuidanceNumber()+1;
        guidance.setGuidanceNumber(number);
        guidanceService.updateGuidanceNumber(guidance);

        favoritesService.addFavorites(favorites);

        return "1";
    }

    /**
     * 删除指导收藏
     *
     * @param request 请求
     * @return {@link String}
     */
    @RequestMapping("/delGuidanceFavorites")
    @ResponseBody
    public String delGuidanceFavorites(HttpServletRequest request){
        HttpSession session= request.getSession();
        Favorites favorites = (Favorites) session.getAttribute("favorites");

        Guidance guidance=guidanceService.getGuidanceById(favorites.getFavoritesGuidance());
        Integer number=guidance.getGuidanceNumber()-1;
        guidance.setGuidanceNumber(number);
        guidanceService.updateGuidanceNumber(guidance);

        favoritesService.removeFavorites(favorites);

        return "1";
    }


    /**
     * 找到用户和用户id
     * 收藏用户
     *
     * @param request 请求
     * @param userId  用户id
     * @return {@link String}
     */
    @RequestMapping("/findUserAndUserId")
    @ResponseBody
    public String findUserAndUserId(HttpServletRequest request,@RequestBody Integer userId){
        HttpSession session= request.getSession();
        UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");
        Favorites favorites = new Favorites();
        favorites.setFavoritesUser(userId);
        favorites.setFavoritesUserId(userInfo.getUserId());
        session.setAttribute("favorites", favorites);
        Favorites favorites1=favoritesService.findByUserIdAndOtherId(favorites);
        if (favorites1 != null) {
            return "1";
        }else {
            return "0";
        }
    }

    @RequestMapping("/addUserFavorites")
    @ResponseBody
    public String addUserFavorites(HttpServletRequest request){
        HttpSession session= request.getSession();
        Favorites favorites = (Favorites) session.getAttribute("favorites");

        UserInfo userInfo = userService.findUserByUserId(favorites.getFavoritesUser());
        Integer number=userInfo.getUserNumber()+1;
        userInfo.setUserNumber(number);
        userService.updateUserNumber(userInfo);
        favoritesService.addFavorites(favorites);

        return "1";
    }

    /**
     * 删除指导收藏
     *
     * @param request 请求
     * @return {@link String}
     */
    @RequestMapping("/delUserFavorites")
    @ResponseBody
    public String delUserFavorites(HttpServletRequest request){
        HttpSession session= request.getSession();
        Favorites favorites = (Favorites) session.getAttribute("favorites");

        UserInfo userInfo = userService.findUserByUserId(favorites.getFavoritesUser());
        Integer number=userInfo.getUserNumber()-1;
        userInfo.setUserNumber(number);
        userService.updateUserNumber(userInfo);

        favoritesService.removeFavorites(favorites);

        return "1";
    }

}
