package com.zj.marriage_network.controller;

import com.alibaba.fastjson.JSON;
import com.zj.marriage_network.entity.Friend;
import com.zj.marriage_network.entity.Hobby;
import com.zj.marriage_network.entity.UserInfo;
import com.zj.marriage_network.service.FriendsService;
import com.zj.marriage_network.service.HobbyService;
import com.zj.marriage_network.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/10 10:22
 * @Decsription: com.zj.marriage_network.controller
 */

@Controller
@RequestMapping("/friends")
public class FriendController {
    @Autowired
    UserService userService;
    @Autowired
    FriendsService friendsService;

    /**
     * 找到用户和用户id
     * 收藏用户
     *
     * @param request 请求
     * @param userId  用户id
     * @return {@link String}
     */
    @RequestMapping("/findUserAndUserId")
    @ResponseBody
    public String findUserAndUserId(HttpServletRequest request, @RequestBody Integer userId){
        HttpSession session= request.getSession();
        UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");
        Integer userIds = userInfo.getUserId();
        Friend friend=friendsService.findFriendStatus(userId, userIds);
        if (friend != null) {
            return "1";
        }else {
            return "0";
        }
    }

    /**
     * 找发送好友申请的朋友
     *
     * @param request 请求
     * @param userId  用户id
     * @return {@link String}
     */
    @RequestMapping("/findFriendStatusWhenOne")
    @ResponseBody
    public String findFriendStatusWhenOne(HttpServletRequest request, @RequestBody Integer userId){
        HttpSession session= request.getSession();
        UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");
        Integer userIds = userInfo.getUserId();
        Friend friend=friendsService.findFriendStatusWhenOne(userId, userIds);
        if (friend != null) {
            return "1";
        }else {
            return "0";
        }
    }

    /**
     * 添加朋友,用户和用户id
     *
     * @param request 请求
     * @param userId  用户id
     * @return {@link String}
     */
    @RequestMapping("/addFriendByUserAndUserId")
    @ResponseBody
    public String addFriendByUserAndUserId(HttpServletRequest request, @RequestBody Integer userId){
        HttpSession session= request.getSession();
        UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");
        Integer userIds = userInfo.getUserId();
        int res=friendsService.addFriend(userId, userIds);
        if (res >= 1) {
            return "1";
        }else {
            return "0";
        }
    }

    @Autowired
    HobbyService hobbyService;
    /**
     * 通过用户id找到爱好
     *
     * @param userId 用户id
     * @return {@link String}
     */
    @RequestMapping("/findHobbyByUserId")
    @ResponseBody
    public String findHobbyByUserId(@RequestBody Integer userId){
        Hobby hobby=hobbyService.findByUserId(userId);
        String JsonString = JSON.toJSONString(hobby);
        return JsonString;
    }

    /**
     * 找到用户,用户性别或用户星座
     *
     * @param userInfo 用户信息
     * @return {@link String}
     */
    @RequestMapping("/findUserByUserSexOrUserConstellation")
    @ResponseBody
    public String findUserByUserSexOrUserConstellation(UserInfo userInfo,HttpServletRequest request){
        HttpSession session= request.getSession();
        UserInfo user = (UserInfo) session.getAttribute("userInfo");
        Integer userIds = user.getUserId();
        userInfo.setUserId(userIds);
        List<UserInfo> userInfoList=userService.findUserByUserSexOrUserConstellation(userInfo);
        String JsonString = JSON.toJSONString(userInfoList);
        return JsonString;
    }

    /**
     * 更新爱好
     *
     * @param hobby 爱好
     * @return {@link String}
     */
    @RequestMapping("/updateHobby")
    @ResponseBody
    public String updateHobby(@RequestBody Hobby hobby){
        hobbyService.updateHobby(hobby);
        return "0";
    }

    /**
     * 添加爱好
     *
     * @param hobby 爱好
     * @return {@link String}
     */
    @RequestMapping("/addHobby")
    @ResponseBody
    public String addHobby(@RequestBody Hobby hobby){
        hobbyService.addHobby(hobby);
        return "0";
    }

    /**
     * 找到我所有朋友状态
     *
     * @param userId 用户id
     * @return {@link String}
     */
    @RequestMapping("/findMyAllFriendByStatusApply")
    @ResponseBody
    public String findMyAllFriendByStatusApply(@RequestBody Integer userId){
        List<Friend> friendList = friendsService.findMyAllFriendByStatusApply(userId);
        String JsonString = JSON.toJSONString(friendList);
        return JsonString;
    }

    /**
     * 朋友状态通过id和状态更新
     *
     * @param friend 朋友
     * @return {@link String}
     */
    @RequestMapping("/updateFriendStatusByIdAndStatus")
    @ResponseBody
    public String updateFriendStatusByIdAndStatus(Friend friend){
        System.out.println(friend);
        int res=friendsService.updateFriendStatus(friend);
        if (res >= 1) {
            return "1";
        }else {
            return "0";
        }
    }
}
