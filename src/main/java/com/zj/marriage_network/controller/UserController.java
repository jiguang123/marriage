package com.zj.marriage_network.controller;

import com.alibaba.fastjson.JSON;
import com.zj.marriage_network.entity.*;
import com.zj.marriage_network.service.*;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/8 15:46
 * @Decsription: com.zj.marriage_network.controller
 */
@Controller
@RequestMapping("/users")
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    ShareService shareService;
    @Autowired
    ActivityService activityService;
    @Autowired
    GuidanceService guidanceService;

    /**
     * 注册页面
     *
     * @return {@link String}
     */
    @RequestMapping("/toRegisterPage")
    public String toRegisterPage(){
        return "/users/register";
    }

    /**
     * 用户登录名找到用户
     *
     * @param loginName 登录名
     * @return {@link String}
     */
    @RequestMapping("/findUserByLoginName")
    @ResponseBody
    public String findUserByLoginName(String loginName){
        UserInfo userInfo= userService.findUserByLoginName(loginName);
        if(userInfo==null){
            return "0";
        }else{
            return "1";
        }
    }

    /**
     * 添加用户
     *
     * @param userInfo 用户信息
     * @return {@link String}
     */
    @RequestMapping("/addUser")
    @ResponseBody
    public String addUser(UserInfo userInfo){
//        System.out.println(userInfo);
        int res= userService.addUser(userInfo);
        if(res >= 1){
            return "1";
        }else{
            return "0";
        }
    }

    /*忘记密码*/

    /**
     * 忘记密码页面
     *
     * @return {@link String}
     */
    @RequestMapping("/toForgotPasswordPage")
    public String toForgotPasswordPage(){
        return "/users/forgot/forgotPassword";
    }

    /**
     * 找到用户名字和邮件
     * 找到名字和邮件
     *
     * @param user 用户
     * @return {@link String}
     */
    @RequestMapping("/findUserByNameAndMail")
    @ResponseBody
    public String findUserByNameAndMail(UserInfo user,HttpSession session) {
        UserInfo userInfo = userService.findUserByNameAndPhoneOrEmail(user);
        session.setAttribute("user", userInfo);
        if(userInfo==null) {
            return "0";
        }else {
            return "1";
        }
    }

    /**
     * 测试和验证
     *
     * @param request 请求
     * @param user    用户
     * @return {@link String}
     */
    @RequestMapping("/testAndVerify")
    @ResponseBody
    public String testAndVerify(HttpServletRequest request,UserInfo user){
        HttpSession session=request.getSession();
        UserInfo userInfo= (UserInfo) session.getAttribute("user");
        if (userInfo.getUserProblem1().equals(user.getUserProblem1())&&
                userInfo.getUserProblem2().equals(user.getUserProblem2())&&
                userInfo.getUserProblem3().equals(user.getUserProblem3())) {
            return "1";
        }else {
            return "0";
        }
    }


    /**
     * 更新用户id密码
     *
     * @param request  请求
     * @param userInfo 用户信息
     * @return {@link String}
     */
    @RequestMapping("/updateUserPasswordById")
    @ResponseBody
    public String updateUserPasswordById(HttpServletRequest request, UserInfo userInfo){
        HttpSession session=request.getSession();
        UserInfo user= (UserInfo) session.getAttribute("user");
        if(user==null){
            user= (UserInfo) session.getAttribute("userInfo");
        }
        user.setLoginPassword(userInfo.getLoginPassword());
        int res=userService.updateUserPassword(user);
        if (res >= 1){
            return "1";
        }else{
            return "0";
        }
    }

    /**
     * 去登录页面
     *
     * @return {@link String}
     */
    @RequestMapping("/toLogin")
    public String toLogin(HttpSession session){
        Numbers numbers=new Numbers();
        numbers.setUserNumber(userService.allUsersCount());
        numbers.setShareNumber(shareService.allCount());
        numbers.setActivityNumber(activityService.allCount());
        numbers.setGuidanceNumber(guidanceService.allCount());
        session.setAttribute("numbers", numbers);
        session.removeAttribute("userInfo");
        return "/users/userLogin";
    }

    /**
     * 找到用户登录名和密码
     *
     * @param loginName 登录名
     * @param loginPassword  密码
     * @return {@link String}
     */
    @RequestMapping("/findUserByLoginNameAndPassword")
    @ResponseBody
    public String findUserByLoginNameAndPassword(String loginName, String loginPassword, HttpSession session){
        UserInfo userInfo = userService.findUserByNameAndPassword(loginName, loginPassword);
        session.setAttribute("userInfo", userInfo);
        if (userInfo == null) {
            return "3";
        }else if (userInfo.getUserStatus() == 0){
            return "0";
        }else if (userInfo.getUserStatus() == 1){
            return "1";
        }else {
            return "2";
        }
    }

    /**
     * 去首页
     *
     * @return {@link String}
     */
    @RequestMapping("/toIndex")
    public String toIndex(HttpSession session){
        // TODO Auto-
        UserInfo userInfo = new UserInfo();
        userInfo.setUserId(1);
        session.setAttribute("userInfo", userInfo);
        return "/users/userIndex";
    }


    /*爱情指导*/

    /**
     * 去爱情指导页面
     *
     * @return {@link String}
     */
    @RequestMapping("/toLoveGuideListPage")
    public String toLoveGuidePage(){
        return "/users/guide/loveGuideList";
    }

    /**
     * 找到所有指导
     *
     * @return {@link String}
     */
    @RequestMapping("/findAllGuidance")
    @ResponseBody
    public String findAllGuidance(){
        List<Guidance> guidanceList= guidanceService.findAllGuidance();
        String JsonString= JSON.toJSONString(guidanceList);
        return JsonString;
    }

    @RequestMapping("/setId")
    @ResponseBody
    public String setId(Integer id,HttpSession session){
        session.setAttribute("id",id);
        return "success";
    }


    @Autowired
    FavoritesService favoritesService;
    /**
     * 通过id找到指导
     *
     * @param request 请求
     * @param model   模型
     * @return {@link String}
     */
    @RequestMapping("/findGuidanceById")
    public String findGuidanceById(HttpServletRequest request,Model model){
        HttpSession session= request.getSession();
        Integer guidanceId= (Integer) session.getAttribute("id");
        Guidance guidance= guidanceService.getGuidanceById(guidanceId);
        model.addAttribute("guidance",guidance);
        return "/users/guide/guidance";
    }



    /**
     * 用户信息页面
     *
     * @return {@link String}
     */
    @RequestMapping("/toUserInfo")
    public String toUserInfoPage(){
        return "/users/userInfo";
    }


    /**
     * 找到用户列表页面
     * @return {@link String}
     */
    @RequestMapping("/toFindUserListPage")
    public String toFindUserListPage(){
        return "/users/friends/usersList";
    }

    /**
     * 找到用户列表
     *
     * @return {@link String}
     */
    @RequestMapping("/toFindUserList")
    @ResponseBody
    public String toFindUserList(HttpServletRequest request){
        HttpSession session= request.getSession();
        UserInfo userInfo= (UserInfo) session.getAttribute("userInfo");
        List<UserInfo> userInfoList=userService.findUserOnline(userInfo.getUserId());
        String JsonString= JSON.toJSONString(userInfoList);
        return JsonString;
    }

    /**
     * 通过用户id找到用户信息
     * 交友模块
     *
     * @param userId 用户id
     * @return {@link ModelAndView}
     */
    @RequestMapping("/toFindUserByUserIdForFriend")
    public ModelAndView toFindUserByUserIdForFriend(Integer userId){
        ModelAndView model=new ModelAndView();
        model.addObject("userId",userId);
        model.setViewName("/users/friends/userListInfo");
        return model;
    }

    /**
     * 为朋友找到用户用户id
     *
     * @param userId 用户id
     * @return {@link String}
     */
    @RequestMapping("/findUserByUserIdForFriend")
    @ResponseBody
    public String findUserByUserIdForFriend(@RequestBody Integer userId){
        UserInfo userInfo=userService.findUserByUserId(userId);
        String JsonString = JSON.toJSONString(userInfo);
        return JsonString;
    }

    /**
     * 错误页面
     *
     * @return {@link String}
     */
    @RequestMapping("/toErrorPage")
    public String toErrorPage(){
        return "/users/error";
    }


    /**
     * 用用户id找到用户信息
     *
     * @param userId 用户id
     * @return {@link String}
     */
    @RequestMapping("/findUserByUserIdToUserInfo")
    @ResponseBody
    public String findUserByUserIdToUserInfo(@RequestBody Integer userId){
        UserInfo userInfo=userService.findUserByUserId(userId);
        String JsonString = JSON.toJSONString(userInfo);
        return JsonString;
    }

    /**
     * 设置日期
     *
     * @param date    日期
     * @param session 会话
     * @return {@link String}
     * @throws ParseException 解析异常
     */
    @RequestMapping("/setDate")
    @ResponseBody
    public String setDate(@RequestBody String date,HttpSession session) throws ParseException {
        //将string类型的时间转换为util
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date dates= format.parse(date);
        //util.Date转换sql.Date
        java.sql.Date sqlDate = new java.sql.Date(dates.getTime());

        session.setAttribute("date",sqlDate);
        return "success";
    }

    /**
     * 更新用户id
     *
     * @param request  请求
     * @param userInfo 用户信息
     * @return {@link String}
     */
    @RequestMapping("/updateUserById")
    @ResponseBody
    public String updateUserById(HttpServletRequest request,@RequestBody UserInfo userInfo){
        HttpSession session= request.getSession();
        java.sql.Date date = (java.sql.Date) session.getAttribute("date");
        userInfo.setUserBirthday(date);
        int res=userService.updateUser(userInfo);
        if (res >= 1){
            return "1";
        }else{
            return "0";
        }
    }

    /**
     * 删除用户
     *
     * @param userId 用户id
     * @return {@link String}
     */
    @RequestMapping("/removeUser")
    @ResponseBody
    public String removeUser(@RequestBody Integer userId){
        UserInfo userInfo=new UserInfo();
        userInfo.setUserId(userId);
        int res=userService.removeUser(userInfo);
        if (res >= 1){
            return "1";
        }else{
            return "0";
        }
    }

    /**
     * 完整用户信息
     *
     * @param userInfo 用户信息
     * @param session  会话
     * @return {@link String}
     */
    @RequestMapping("/completeUserInformation")
    public String completeUserInformation(UserInfo userInfo,HttpSession session){
        UserInfo userInfos = userService.findUserByNameAndPassword(userInfo.getLoginName(), userInfo.getLoginPassword());
        session.setAttribute("userInfo", userInfos);
        return "/users/wanSanXX";
    }

    @RequestMapping("/toFeedbackPage")
    public String toFeedbackPage(){
        return "/users/Feedback/addFeedback";
    }
    @RequestMapping("/toActivityPage")
    public String totoActivityPage(){
        return "/users/activity/activityList";
    }
    @RequestMapping("/toMyFriendsListPage")
    public String toMyFriendsListPage(){
        return "/users/friends/myFriendsList";
    }
    @RequestMapping("/toShareListPage")
    public String toShareListPage(){
        return "/users/share/shareList";
    }
    @RequestMapping("/toMyFavoritesListPage")
    public String toMyFavoritesListPage(){
        return "/users/favorites/myFavoritesList";
    }
}
