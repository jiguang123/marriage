package com.zj.marriage_network.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import lombok.Data;

/**
 * 
 * @TableName mn_activity
 */
@TableName(value ="mn_activity")
@Data
public class Activity implements Serializable {
    /**
     * 活动id
     */
    @TableId(value = "activity_id", type = IdType.AUTO)
    private Integer activityId;

    /**
     * 活动标题
     */
    @TableField(value = "activity_title")
    private String activityTitle;

    /**
     * 活动内容
     */
    @TableField(value = "activity_content")
    private String activityContent;

    /**
     * 活动开始时间
     */
    @TableField(value = "activity_start_time")
    private Date activityStartTime;

    /**
     * 活动结束时间
     */
    @TableField(value = "activity_end_time")
    private Date activityEndTime;

    /**
     * 活动地点
     */
    @TableField(value = "activity_address")
    private String activityAddress;

    /**
     * 活动人数
     */
    @TableField(value = "activity_number")
    private Integer activityNumber;

    /**
     * 活动空余量
     */
    @TableField(value = "activity_spare")
    private Integer activitySpare;

    /**
     * 活动费用
     */
    @TableField(value = "activity_fee")
    private BigDecimal activityFee;

    /**
     * 收藏量
     */
    @TableField(value = "activity_like")
    private Integer activityLike;

    /**
     * 活动状态，0未开始，1进行中，2已结束，3删除
     */
    @TableField(value = "activity_status")
    private Integer activityStatus;

    /**
     * 保留1
     */
    @TableField(value = "activity_other1")
    private String activityOther1;

    /**
     * 保留2
     */
    @TableField(value = "activity_other2")
    private String activityOther2;

    /**
     * 保留3
     */
    @TableField(value = "activity_other3")
    private String activityOther3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}