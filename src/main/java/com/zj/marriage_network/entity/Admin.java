package com.zj.marriage_network.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @author lx
 * @TableName mn_admin
 */
@TableName(value ="mn_admin")
@Data
public class Admin implements Serializable {
    /**
     * //管理员id
     */
    @TableId(value = "admin_id", type = IdType.AUTO)
    private Integer adminId;

    /**
     * 登录名称
     */
    @TableField(value = "admin_name")
    private String adminName;

    /**
     * 密码
     */
    @TableField(value = "admin_password")
    private String adminPassword;

    /**
     * 邮箱
     */
    @TableField(value = "admin_mail")
    private String adminMail;

    /**
     * 密保问题1 喜欢的偶像
     */
    @TableField(value = "admin_question1")
    private String adminQuestion1;

    /**
     * 密保问题2 喜欢的歌
     */
    @TableField(value = "admin_question2")
    private String adminQuestion2;

    /**
     * 密保问题3 喜欢的运动
     */
    @TableField(value = "admin_question3")
    private String adminQuestion3;

    /**
     * 保留1
     */
    @TableField(value = "admin_other1")
    private String adminOther1;

    /**
     * 保留2
     */
    @TableField(value = "admin_other2")
    private String adminOther2;

    /**
     * 保留3
     */
    @TableField(value = "admin_other3")
    private String adminOther3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}