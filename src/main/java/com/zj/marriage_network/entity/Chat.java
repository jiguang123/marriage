package com.zj.marriage_network.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName mn_chat
 */
@TableName(value ="mn_chat")
@Data
public class Chat implements Serializable {
    /**
     * 信息id
     */
    @TableId(value = "chat_id", type = IdType.AUTO)
    private Integer chatId;

    /**
     * 发消息的用户
     */
    @TableField(value = "chat_user")
    private Integer chatUser;

    /**
     * 该条消息属于哪对朋友
     */
    @TableField(value = "chat_friend")
    private Integer chatFriend;

    /**
     * 消息内容
     */
    @TableField(value = "chat_content")
    private String chatContent;

    /**
     * 发送时间
     */
    @TableField(value = "chat_time")
    private Date chatTime;

    /**
     * 状态 0未读，1已读，2删除
     */
    @TableField(value = "chat_status")
    private Integer chatStatus;

    /**
     * 保留1
     */
    @TableField(value = "chat_other1")
    private String chatOther1;

    /**
     * 保留2
     */
    @TableField(value = "chat_other2")
    private String chatOther2;

    /**
     * 保留3
     */
    @TableField(value = "chat_other3")
    private String chatOther3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}