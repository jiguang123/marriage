package com.zj.marriage_network.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName mn_discussion
 */
@TableName(value ="mn_discussion")
@Data
public class Discussion implements Serializable {
    /**
     * 
     */
    @TableId(value = "dis_id", type = IdType.AUTO)
    private Integer disId;

    /**
     * 内容
     */
    @TableField(value = "dis_content")
    private String disContent;

    /**
     * 时间
     */
    @TableField(value = "dis_time")
    private Date disTime;

    /**
     * 发布人
     */
    @TableField(value = "dis_user")
    private Integer disUser;

    /**
     * 分享
     */
    @TableField(value = "dis_share")
    private Integer disShare;

    /**
     * 保留1
     */
    @TableField(value = "dis_other1")
    private String disOther1;

    /**
     * 保留2
     */
    @TableField(value = "dis_other2")
    private String disOther2;

    /**
     * 保留3
     */
    @TableField(value = "dis_other3")
    private String disOther3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}