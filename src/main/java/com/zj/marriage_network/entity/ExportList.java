package com.zj.marriage_network.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Author: ZJ
 * @Date: 2022/9/8 9:37
 * @Decsription: com.zj.marriage_network.entity
 */
@HeadRowHeight(value = 25) // 表头行高
@ContentRowHeight(value = 25) // 内容行高
@ColumnWidth(value = 30) // 列宽
@Data
public class ExportList implements Serializable {
    /**
     * 真实姓名
     */
    @ExcelProperty("用户名")
    @TableField(value = "real_name")
    private String realName;



    /**
     * 电话号码
     */
    @ExcelProperty("电话号码")
    @TableField(value = "user_phone")
    private String userPhone;

    /**
     * 邮箱
     */
    @ExcelProperty("邮箱")
    @TableField(value = "user_mail")
    private String userMail;

    /**
     * 活动标题
     */
    @ExcelProperty("活动主题")
    @TableField(value = "activity_title")
    private String activityTitle;

    /**
     * 费用
     */
    @ExcelProperty("活动费用")
    @TableField(value = "sign_cost")
    private BigDecimal signCost;

}
