package com.zj.marriage_network.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @author lx
 * @TableName mn_favorites
 */
@TableName(value ="mn_favorites")
@Data
public class Favorites implements Serializable {
    /**
     * 
     */
    @TableId(value = "favorites_id", type = IdType.AUTO)
    private Integer favoritesId;

    /**
     * 提交收藏的用户id
     */
    @TableField(value = "favorites_user_id")
    private Integer favoritesUserId;

    /**
     * 被收藏的用户id
     */
    @TableField(value = "favorites_user")
    private Integer favoritesUser;

    /**
     * 被收藏的活动id
     */
    @TableField(value = "favorites_activity")
    private Integer favoritesActivity;

    /**
     * 被点赞的分享的id
     */
    @TableField(value = "favorites_share")
    private Integer favoritesShare;

    /**
     * 被收藏的恋爱指导的id
     */
    @TableField(value = "favorites_guidance")
    private Integer favoritesGuidance;

    /**
     * 保留1
     */
    @TableField(value = "favorites_other1")
    private String favoritesOther1;

    /**
     * 保留2
     */
    @TableField(value = "favorites_other2")
    private String favoritesOther2;

    /**
     * 保留3
     */
    @TableField(value = "favorites_other3")
    private String favoritesOther3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}