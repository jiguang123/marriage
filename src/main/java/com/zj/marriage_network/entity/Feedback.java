package com.zj.marriage_network.entity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.sql.Date;
import lombok.Data;

/**
 * 
 * @TableName mn_feedback
 */
@TableName(value ="mn_feedback")
@Data
public class Feedback implements Serializable {
    /**
     * 
     */
    @TableId(value = "feedback_id")
    private Integer feedbackId;

    /**
     * 标题
     */
    @TableField(value = "feedback_title")
    private String feedbackTitle;

    /**
     * 内容
     */
    @TableField(value = "feedback_content")
    private String feedbackContent;

    /**
     * 时间
     */
    @TableField(value = "feedback_time")
    private Date feedbackTime;

    /**
     * 用户
     */
    @TableField(value = "feedback_user")
    private Integer feedbackUser;

    /**
     * 状态    未读0,已读1，删除2
     */
    @TableField(value = "feedback_status")
    private Integer feedbackStatus;

    /**
     * 保留1
     */
    @TableField(value = "feedback_other1")
    private String feedbackOther1;

    /**
     * 保留2
     */
    @TableField(value = "feedback_other2")
    private String feedbackOther2;

    /**
     * 保留3
     */
    @TableField(value = "feedback_other3")
    private String feedbackOther3;

    private UserInfo userInfo;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}