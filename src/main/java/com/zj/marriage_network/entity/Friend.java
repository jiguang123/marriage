package com.zj.marriage_network.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName mn_friend
 */
@TableName(value ="mn_friend")
@Data
public class Friend implements Serializable {
    /**
     * 好友关系id
     */
    @TableId(value = "friend_id", type = IdType.AUTO)
    private Integer friendId;

    /**
     * 用户1
     */
    @TableField(value = "friend_user_one")
    private Integer friendUserOne;

    private UserInfo userOne;

    /**
     * 用户2
     */
    @TableField(value = "friend_user_two")
    private Integer friendUserTwo;

    private UserInfo userTwo;

    /**
     * 好友状态；申请中1、通过2、删除3
     */
    @TableField(value = "friend_status")
    private Integer friendStatus;

    /**
     * 保留1
     */
    @TableField(value = "friend_other1")
    private String friendOther1;

    /**
     * 保留2
     */
    @TableField(value = "friend_other2")
    private String friendOther2;

    /**
     * 保留3
     */
    @TableField(value = "friend_other3")
    private String friendOther3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}