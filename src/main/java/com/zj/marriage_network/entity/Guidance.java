package com.zj.marriage_network.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.sql.Date;

import lombok.Data;

/**
 * 
 * @TableName mn_guidance
 */
@TableName(value ="mn_guidance")
@Data
public class Guidance implements Serializable {
    /**
     * 
     */
    @TableId(value = "guidance_id", type = IdType.AUTO)
    private Integer guidanceId;

    /**
     * 标题
     */
    @TableField(value = "guidance_title")
    private String guidanceTitle;

    /**
     * 作者
     */
    @TableField(value = "guidance_author")
    private String guidanceAuthor;

    /**
     * 内容
     */
    @TableField(value = "guidance_content")
    private String guidanceContent;

    /**
     * 时间
     */
    @TableField(value = "guidance_time")
    private Date guidanceTime;

    /**
     * 转载于
     */
    @TableField(value = "guidance_reproduced")
    private String guidanceReproduced;

    /**
     * 收藏量
     */
    @TableField(value = "guidance_number")
    private Integer guidanceNumber;

    /**
     * 保留1
     */
    @TableField(value = "guidance_other1")
    private String guidanceOther1;

    /**
     * 保留2
     */
    @TableField(value = "guidance_other2")
    private String guidanceOther2;

    /**
     * 保留3
     */
    @TableField(value = "guidance_other3")
    private String guidanceOther3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}