package com.zj.marriage_network.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName mn_hobby
 */
@TableName(value ="mn_hobby")
@Data
public class Hobby implements Serializable {
    /**
     * 用户ID
     */
    @TableId(value = "hobby_user")
    private Integer hobbyUser;

    /**
     * 喜欢的菜
     */
    @TableField(value = "hobby_dish")
    private String hobbyDish;

    /**
     * 喜欢的歌
     */
    @TableField(value = "hobby_song")
    private String hobbySong;

    /**
     * 喜欢的书
     */
    @TableField(value = "hobby_book")
    private String hobbyBook;

    /**
     * 喜欢的运动
     */
    @TableField(value = "hobby_sports")
    private String hobbySports;

    /**
     * 喜欢的一句话
     */
    @TableField(value = "hobby_famous_quote")
    private String hobbyFamousQuote;

    /**
     * 保留1
     */
    @TableField(value = "hobby_other1")
    private String hobbyOther1;

    /**
     * 保留2
     */
    @TableField(value = "hobby_other2")
    private String hobbyOther2;

    /**
     * 保留3
     */
    @TableField(value = "hobby_other3")
    private String hobbyOther3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}