package com.zj.marriage_network.entity;

import lombok.Data;

/**
 * @Author: ZJ
 * @Date: 2022/9/6 23:42
 * @Decsription: com.zj.marriage_network.entity
 */
@Data
public class Link {
    private Integer linkId;
    private String linkName;
    private String linkImg;
    private String linkUrl;
    private Integer linkStatus;  //状态，1启用，0删除
}
