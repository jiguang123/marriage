package com.zj.marriage_network.entity;

import lombok.Data;

/**
 * @Author: ZJ
 * @Date: 2022/9/6 9:33
 * @Decsription: com.zj.marriage_network.entity
 */
@Data
public class Numbers {
    private int userNumber;
    private int shareNumber;
    private int activityNumber;
    private int guidanceNumber;
    private int feedbackNumber;
    private int linkNumber;
}
