package com.zj.marriage_network.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName mn_photo
 */
@TableName(value ="mn_photo")
@Data
public class Photo implements Serializable {
    /**
     * 图片ID
     */
    @TableId(value = "photo_id", type = IdType.AUTO)
    private Integer photoId;

    /**
     * 图片地址
     */
    @TableField(value = "photo_url")
    private String photoUrl;

    /**
     * 图片描述
     */
    @TableField(value = "photo_description")
    private String photoDescription;

    /**
     * 图片属于那个用户
     */
    @TableField(value = "photo_user")
    private Integer photoUser;

    /**
     * 保留1
     */
    @TableField(value = "photo_other1")
    private String photoOther1;

    /**
     * 保留2
     */
    @TableField(value = "photo_other2")
    private String photoOther2;

    /**
     * 保留3
     */
    @TableField(value = "photo_other3")
    private String photoOther3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}