package com.zj.marriage_network.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName mn_role
 */
@TableName(value ="mn_role")
@Data
public class Role implements Serializable {
    /**
     * 角色id
     */
    @TableId(value = "role_id", type = IdType.AUTO)
    private Integer roleId;

    /**
     * 角色名称  普通用户，vip用户
     */
    @TableField(value = "role_name")
    private String roleName;

    /**
     * 角色描述
     */
    @TableField(value = "role_description")
    private String roleDescription;

    /**
     * 保留1
     */
    @TableField(value = "role_other1")
    private String roleOther1;

    /**
     * 保留2
     */
    @TableField(value = "role_other2")
    private String roleOther2;

    /**
     * 保留3
     */
    @TableField(value = "role_other3")
    private String roleOther3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}