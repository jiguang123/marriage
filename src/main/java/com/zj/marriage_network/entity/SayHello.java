package com.zj.marriage_network.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.sql.Date;
import lombok.Data;

/**
 * 
 * @author lx
 * @TableName mn_say_hello
 */
@TableName(value ="mn_say_hello")
@Data
public class SayHello implements Serializable {
    /**
     * 信息id
     */
    @TableId(value = "say_id", type = IdType.AUTO)
    private Integer sayId;

    /**
     * 发用户1
     */
    @TableField(value = "say_user_one")
    private Integer sayUserOne;

    private UserInfo userOne;

    /**
     * 收用户2
     */
    @TableField(value = "say_user_two")
    private Integer sayUserTwo;

    /**
     * 消息内容
     */
    @TableField(value = "say_content")
    private String sayContent;

    /**
     * 发送时间
     */
    @TableField(value = "say_time")
    private Date sayTime;

    /**
     * 状态 0未读，1已读，2删除
     */
    @TableField(value = "say_status")
    private Integer sayStatus;

    /**
     * 保留1
     */
    @TableField(value = "say_other1")
    private String sayOther1;

    /**
     * 保留2
     */
    @TableField(value = "say_other2")
    private String sayOther2;

    /**
     * 保留3
     */
    @TableField(value = "say_other3")
    private String sayOther3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}