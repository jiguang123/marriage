package com.zj.marriage_network.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;

/**
 * 
 * @TableName mn_selection_condition
 */
@TableName(value ="mn_selection_condition")
@Data
public class SelectionCondition implements Serializable {
    /**
     * 年龄
     */
    @TableField(value = "sc_age")
    private Integer scAge;

    /**
     * 地址
     */
    @TableField(value = "sc_address")
    private String scAddress;

    /**
     * 身高
     */
    @TableField(value = "sc_height")
    private BigDecimal scHeight;

    /**
     * 用户ID
     */
    @TableField(value = "sc_user")
    private Integer scUser;

    /**
     * 保留1
     */
    @TableField(value = "sc_other1")
    private String scOther1;

    /**
     * 保留2
     */
    @TableField(value = "sc_other2")
    private String scOther2;

    /**
     * 保留3
     */
    @TableField(value = "sc_other3")
    private String scOther3;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}