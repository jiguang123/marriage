package com.zj.marriage_network.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.sql.Date;
import lombok.Data;

/**
 * 
 * @TableName mn_share
 */
@TableName(value ="mn_share")
@Data
public class Share implements Serializable {
    /**
     * 分享id
     */
    @TableId(value = "share_id", type = IdType.AUTO)
    private Integer shareId;

    /**
     * 分享标题
     */
    @TableField(value = "share_title")
    private String shareTitle;

    /**
     * 分享内容
     */
    @TableField(value = "share_content")
    private String shareContent;

    /**
     * 分享图片
     */
    @TableField(value = "share_image")
    private String shareImage;

    /**
     * 分享时间
     */
    @TableField(value = "share_time")
    private Date shareTime;

    /**
     * 分享人
     */
    @TableField(value = "share_user")
    private Integer shareUser;

    /**
     * 点赞数
     */
    @TableField(value = "share_like")
    private Integer shareLike;

    /**
     * 状态 0待审核，1发布，2审核未通过，3删除
     */
    @TableField(value = "share_status")
    private Integer shareStatus;

    /**
     * 保留1
     */
    @TableField(value = "share_other1")
    private String shareOther1;

    /**
     * 保留2
     */
    @TableField(value = "share_other2")
    private String shareOther2;

    /**
     * 保留3
     */
    @TableField(value = "share_other3")
    private String shareOther3;

    private UserInfo userInfo;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}