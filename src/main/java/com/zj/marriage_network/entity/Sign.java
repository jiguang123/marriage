package com.zj.marriage_network.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import lombok.Data;

/**
 * 
 * @TableName mn_sign
 */
@TableName(value ="mn_sign")
@Data
public class Sign implements Serializable {
    /**
     * 
     */
    @TableId(value = "sign_id", type = IdType.AUTO)
    private Integer signId;

    /**
     * 用户
     */
    @TableField(value = "sign_user")
    private Integer signUser;

    /**
     * 活动
     */
    @TableField(value = "sign_activity")
    private Integer signActivity;

    /**
     * 费用
     */
    @TableField(value = "sign_cost")
    private BigDecimal signCost;

    /**
     * 保留1
     */
    @TableField(value = "sign_other1")
    private String signOther1;

    /**
     * 保留2
     */
    @TableField(value = "sign_other2")
    private String signOther2;

    /**
     * 保留3
     */
    @TableField(value = "sign_other3")
    private String signOther3;

    private UserInfo userInfo;

    private Activity activity;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}