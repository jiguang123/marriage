package com.zj.marriage_network.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import lombok.Data;

/**
 * 
 * @author lx
 * @TableName mn_user
 */
@TableName(value ="mn_user")
@Data
public class UserInfo implements Serializable {
    /**
     * 用户id
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Integer userId;

    /**
     * 登录名称
     */
    @TableField(value = "login_name")
    private String loginName;

    /**
     * 登录密码
     */
    @TableField(value = "login_password")
    private String loginPassword;

    /**
     * 用户姓名
     */
    @TableField(value = "user_name")
    private String userName;

    /**
     * 真实姓名
     */
    @TableField(value = "real_name")
    private String realName;

    /**
     * 性别1,2来表示，男,女
     */
    @TableField(value = "user_sex")
    private Integer userSex;

    /**
     * 年龄
     */
    @TableField(value = "user_age")
    private Integer userAge;

    /**
     * 生日
     */
    @TableField(value = "user_birthday")
    private Date userBirthday;

    /**
     * 星座
     */
    @TableField(value = "user_constellation")
    private String userConstellation;

    /**
     * 学历
     */
    @TableField(value = "user_education")
    private String userEducation;

    /**
     * 工作
     */
    @TableField(value = "user_job")
    private String userJob;

    /**
     * 电话号码
     */
    @TableField(value = "user_phone")
    private String userPhone;

    /**
     * 邮箱
     */
    @TableField(value = "user_mail")
    private String userMail;

    /**
     * 工资
     */
    @TableField(value = "user_salary")
    private BigDecimal userSalary;

    /**
     * 婚姻状态  ：未婚、已婚、离异、丧偶
     */
    @TableField(value = "user_marital_status")
    private String userMaritalStatus;

    /**
     * 身高
     */
    @TableField(value = "user_height")
    private BigDecimal userHeight;

    /**
     * 现居地址
     */
    @TableField(value = "user_address")
    private String userAddress;

    /**
     * 头像
     */
    @TableField(value = "user_avatar")
    private String userAvatar;

    /**
     * 角色
     */
    @TableField(value = "user_role")
    private Integer userRole;

    /**
     * 被收藏量
     */
    @TableField(value = "user_number")
    private Integer userNumber;

    /**
     * 是否被禁用		0未禁用，1禁用
     */
    @TableField(value = "user_status")
    private Integer userStatus;

    /**
     * 是否被删除		0未删除，1删除
     */
    @TableField(value = "is_delete")
    private Integer delete;

    /**
     * 账号启用时间
     */
    @TableField(value = "registration_time")
    private Date registrationTime;

    /**
     * 账号封号时间
     */
    @TableField(value = "user_title_time")
    private Date userTitleTime;

    /**
     * 账号注销时间
     */
    @TableField(value = "user_logout_time")
    private Date userLogoutTime;


    /**
     * 密保1
     */
    @TableField(value = "user_problem1")
    private String userProblem1;
    /**
     * 密保2
     */
    @TableField(value = "user_problem2")
    private String userProblem2;
    /**
     * 密保3
     */
    @TableField(value = "user_problem3")
    private String userProblem3;
    /**
     * 保留1
     */
    @TableField(value = "user_other1")
    private String userOther1;

    /**
     * 保留2
     */
    @TableField(value = "user_other2")
    private String userOther2;

    /**
     * 保留3
     */
    @TableField(value = "user_other3")
    private String userOther3;


    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}