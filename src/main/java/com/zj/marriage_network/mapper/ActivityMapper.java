package com.zj.marriage_network.mapper;

import com.zj.marriage_network.entity.Activity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/5 23:39
 * @Decsription: com.zj.marriage_network.mapper
 */
@Mapper
public interface ActivityMapper {
    /**
     * 所有条数
     *
     * @return int
     */
    int allCount();

    /**
     * 找到所有活动
     *
     * @return {@link List}<{@link Activity}>
     */
    List<Activity> findAllActivity();

    /**
     * 找到活动id
     *
     * @param activityId 活动id
     * @return {@link List}<{@link Activity}>
     */
    Activity findActivityById(Integer activityId);

    /**
     * 找到活动状态
     *
     * @param activityStatus 活动状态
     * @return {@link List}<{@link Activity}>
     */
    List<Activity> findActivityByStatus(Integer activityStatus);

    /**
     * 添加活动
     *
     * @param activity 活动
     * @return int
     */
    int addActivity(Activity activity);

    /**
     * 删除活动
     *
     * @param activity 活动
     * @return int
     */
    int removeActivity(Activity activity);

    /**
     * 更新活动
     *
     * @param activity 活动
     * @return int
     */
    int updateActivity(Activity activity);

    /**
     * 更新活动状态
     *
     * @param activity 活动
     * @return int
     */
    int updateActivityStatus(Activity activity);

    /**
     * 更新活动的被收藏量
     *
     * @param activity 活动
     * @return int
     */
    int updateActivityLike(Activity activity);

    /**
     * 更新活动空余量
     *
     * @param activity 活动
     * @return int
     */
    int updateActivitySpare(Activity activity);

}
