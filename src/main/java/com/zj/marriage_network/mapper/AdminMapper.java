package com.zj.marriage_network.mapper;

import com.zj.marriage_network.entity.Admin;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: ZJ
 * @Date: 2022/9/5 9:31
 * @Decsription: com.zj.marriage_network.mapper
 */
@Mapper
public interface AdminMapper {
    /**
     * 找到管理通过id
     *
     * @param id id
     * @return {@link Admin}
     */
    Admin findAdminById(Integer id);


    /**
     * 找到管理名字和密码
     *
     * @param adminName     管理员名称
     * @param adminPassword 管理员密码
     * @return {@link Admin}
     */
    Admin findAdminByNameAndPassword(String adminName, String adminPassword);


    /**
     * 更新管理通过id
     *
     * @param admin 管理
     * @return int
     */
    int updateAdminById(Admin admin);

    /**
     * 更新管理密码通过id
     *
     * @param admin 管理
     * @return int
     */
    int updateAdminPasswordById(Admin admin);

    /**
     * 找到管理名字和邮件
     *
     * @param adminName 管理员名称
     * @param adminMail 管理邮件
     * @return {@link Admin}
     */
    Admin findAdminByNameAndMail(String adminName, String adminMail);
}
