package com.zj.marriage_network.mapper;

import com.zj.marriage_network.entity.Favorites;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/9 16:13
 * @Decsription: com.zj.marriage_network.mapper
 */
@Mapper
public interface FavoritesMapper {

    /**
     * 添加收藏夹
     *
     * @param favorites 最喜欢
     * @return int
     */
    int addFavorites(Favorites favorites);

    /**
     * 删除收藏夹
     *
     * @param favorites 最喜欢
     * @return int
     */
    int removeFavorites(Favorites favorites);

    /**
     * 找到所有收藏
     *
     * @return {@link List}<{@link Favorites}>
     */
    List<Favorites> getFavorites();

    /**
     * 用户id和其他id找到收藏
     *
     * @param favorites 最喜欢
     * @return {@link Favorites}
     */
    Favorites findByUserIdAndOtherId(Favorites favorites);
}
