package com.zj.marriage_network.mapper;

import com.zj.marriage_network.entity.Feedback;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/5 23:49
 * @Decsription: com.zj.marriage_network.mapper
 */
@Mapper
public interface FeedbackMapper {
    /**
     * 所有条数
     *
     * @return int
     */
    int allCount();

    /**
     * 添加反馈
     *
     * @param feedback 反馈
     * @return int
     */
    int addFeedback(Feedback feedback);

    /**
     * 更新反馈状态
     *
     * @param feedback 反馈
     * @return int
     */
    int updateFeedbackStatus(Feedback feedback);

    /**
     * 发现通过反馈id
     *
     * @param feedbackId 反馈id
     * @return {@link Feedback}
     */
    Feedback findByFeedbackId(Integer feedbackId);

    /**
     * 得到反馈列表
     *
     * @return {@link List}<{@link Feedback}>
     */
    List<Feedback> getFeedbackList();


    /**
     * 通过状态找到反馈
     *
     * @param feedbackStatus feedback status
     * @return {@link List}<{@link Feedback}>
     */
    List<Feedback> findFeedbackByStatus(Integer feedbackStatus);
}
