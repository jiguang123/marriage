package com.zj.marriage_network.mapper;

import com.zj.marriage_network.entity.Hobby;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: ZJ
 * @Date: 2022/9/10 14:48
 * @Decsription: com.zj.marriage_network.mapper
 */
@Mapper
public interface HobbyMapper {
    /**
     * 通过用户id找到喜好
     *
     * @param userId 用户id
     * @return {@link Hobby}
     */
    Hobby findByUserId(Integer userId);

    /**
     * 更新爱好
     *
     * @param hobby 爱好
     * @return int
     */
    int updateHobby(Hobby hobby);


    /**
     * 添加爱好
     *
     * @param hobby 爱好
     * @return int
     */
    int addHobby(Hobby hobby);
}
