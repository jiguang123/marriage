package com.zj.marriage_network.mapper;

import com.zj.marriage_network.entity.Link;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/5 23:50
 * @Decsription: com.zj.marriage_network.mapper
 */
@Mapper
public interface LinkMapper {
    /**
     * 所有条数
     *
     * @return int
     */
    @Select("SELECT COUNT(*) FROM mn_link")
    int allCount();

    /**
     * 添加链接
     *
     * @param link 链接
     * @return int
     */
    @Insert("insert into mn_link (link_name, link_img, link_url) values (#{linkName},#{linkImg},#{linkUrl})")
    int addLink(Link link);

    /**
     * 更新链接
     *
     * @param link 链接
     * @return int
     */
    @Update("update mn_link set link_name=#{linkName},link_img=#{linkImg},link_url=#{linkUrl} where link_id=#{linkId}")
    int updateLink(Link link);

    /**
     * 删除链接
     *
     * @param link 链接
     * @return int
     */
    @Update("update mn_link set link_status = 0 where link_id=#{linkId}")
    int deleteLink(Link link);

    /**
     * 找到所有链接
     *
     * @return {@link List}<{@link Link}>
     */
    @Select("select * from mn_link where link_status=1")
    List<Link> findAllLinks();

    /**
     * 发现链接名字
     *
     * @param name 名字
     * @return {@link List}<{@link Link}>
     */
    @Select("select * from mn_link where link_name  like concat('%',#{name},'%') and link_status=1")
    List<Link> findLinksByName(String name);

    /**
     * 找到链接,链接id
     *
     * @param linkId 链接id
     * @return {@link Link}
     */
    @Select("select * from mn_link where link_id = #{linkId}")
    Link findLinkByLinkId(Integer linkId);
}
