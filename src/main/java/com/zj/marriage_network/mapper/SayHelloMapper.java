package com.zj.marriage_network.mapper;

import com.zj.marriage_network.entity.SayHello;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/10 12:46
 * @Decsription: com.zj.marriage_network.mapper
 */
@Mapper
public interface SayHelloMapper {
    /**
     * 找到用户id和其他id
     *
     * @param userId  用户id
     * @param otherId 其他id
     * @return {@link SayHello}
     */
    SayHello findByUserIdAndOtherId(Integer userId, Integer otherId);

    /**
     * 添加问好
     *
     * @param userId  用户id
     * @param otherId 其他id
     * @return int
     */
    int addSayHello(Integer userId, Integer otherId);

    /**
     * 找到所有给我的打招呼
     *
     * @return {@link List}<{@link SayHello}>
     */
    List<SayHello> findAllMySayHello(Integer userId);

    /**
     *找到所有给我的打招呼通过状态
     *
     * @param sayHello 说“你好”
     * @return {@link List}<{@link SayHello}>
     */
    List<SayHello> findAllMySayHelloByStatus(SayHello sayHello);



    /**
     * 更新打招呼的状态
     *
     * @param sayHello 说“你好”
     * @return int
     */
    int updateHelloStatus(SayHello sayHello);

    /**
     * 修改我的所有打招呼的信息
     *
     * @param sayHello 说“你好”
     * @return int
     */
    int updateHelloStatusByMy(SayHello sayHello);

    /**
     * 删除打招呼
     *
     * @param sayId 说id
     * @return int
     */
    int deleteSayHello(Integer sayId);


    /**
     * 我没有看过的消息的数量
     *
     * @param userId 用户id
     * @return int
     */
    int mySayHelloNoLook(Integer userId);

}
