package com.zj.marriage_network.service;

import com.zj.marriage_network.entity.Friend;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/10 10:38
 * @Decsription: com.zj.marriage_network.service
 */
public interface FriendsService {

    /**
     * 找到所有朋友
     *
     * @param myId   我身份证
     * @param yourId 你身份证
     * @return {@link List}<{@link Friend}>
     */
    Friend findFriendStatus(Integer myId,Integer yourId);


    /**
     * 查看是否已经申请过好友
     *
     * @param myId   我身份证
     * @param yourId 你身份证
     * @return {@link Friend}
     */
    Friend findFriendStatusWhenOne(Integer myId,Integer yourId);

    /**
     * 发现我所有朋友根据状态
     *
     * @param userId 用户id
     * @return {@link List}<{@link Friend}>
     */
    List<Friend> findAllFriendMyByStatus(Integer userId);

    /**
     * 找到我所有朋友申请
     *
     * @param userId 用户id
     * @return {@link List}<{@link Friend}>
     */
    List<Friend> findMyAllFriendByStatusApply(Integer userId);

    /**
     * 更新朋友状态
     *
     * @param friend 朋友
     * @return int
     */
    int updateFriendStatus(Friend friend);

    /**
     * 添加朋友
     *
     * @param myId   我身份证
     * @param yourId 你身份证
     * @return int
     */
    int addFriend(Integer myId,Integer yourId);
}
