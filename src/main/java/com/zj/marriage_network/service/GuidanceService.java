package com.zj.marriage_network.service;

import com.zj.marriage_network.entity.Guidance;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/5 23:48
 * @Decsription: com.zj.marriage_network.service
 */
public interface GuidanceService {
    /**
     * 所有条数
     *
     * @return int
     */
    int allCount();


    /**
     * 添加指导
     *
     * @param guidance 指导
     * @return int
     */
    int addGuidance(Guidance guidance);

    /**
     * 更新指导
     *
     * @param guidance 指导
     * @return int
     */
    int updateGuidance(Guidance guidance);

    /**
     * 删除指导
     *
     * @param guidanceId 指导id
     * @return int
     */
    int deleteGuidance(Integer guidanceId);


    /**
     * 更新指导数量
     *
     * @param guidance 指导
     * @return int
     */
    int updateGuidanceNumber(Guidance guidance);

    /**
     * 找到所有指导
     *
     * @return {@link List}<{@link Guidance}>
     */
    List<Guidance> findAllGuidance();

    /**
     * 找到所有指导 模糊查询
     *
     * @param guidance 指导
     * @return {@link List}<{@link Guidance}>
     */
    List<Guidance> findAllGuidanceByLike(Guidance guidance);

    /**
     * 通过id获取指导
     *
     * @param guidanceId 指导id
     * @return {@link Guidance}
     */
    Guidance getGuidanceById(Integer guidanceId);
}
