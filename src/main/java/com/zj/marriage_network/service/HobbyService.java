package com.zj.marriage_network.service;

import com.zj.marriage_network.entity.Hobby;

/**
 * @Author: ZJ
 * @Date: 2022/9/10 14:50
 * @Decsription: com.zj.marriage_network.service
 */
public interface HobbyService {
    /**
     * 通过用户id找到喜好
     *
     * @param userId 用户id
     * @return {@link Hobby}
     */
    Hobby findByUserId(Integer userId);

    /**
     * 更新爱好
     *
     * @param hobby 爱好
     * @return int
     */
    int updateHobby(Hobby hobby);

    /**
     * 添加爱好
     *
     * @param hobby 爱好
     * @return int
     */
    int addHobby(Hobby hobby);
}
