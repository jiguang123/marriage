package com.zj.marriage_network.service;

import com.zj.marriage_network.entity.Link;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/5 23:50
 * @Decsription: com.zj.marriage_network.service
 */
public interface LinkService {
    /**
     * 所有条数
     *
     * @return int
     */
    int allCount();
    /**
     * 添加链接
     *
     * @param link 链接
     * @return int
     */
    int addLink(Link link);

    /**
     * 更新链接
     *
     * @param link 链接
     * @return int
     */
    int updateLink(Link link);

    /**
     * 删除链接
     *
     * @param link 链接
     * @return int
     */
    int deleteLink(Link link);

    /**
     * 找到所有链接
     *
     * @return {@link List}<{@link Link}>
     */
    List<Link> findAllLinks();

    /**
     * 发现链接名字
     *
     * @param name 名字
     * @return {@link List}<{@link Link}>
     */
    List<Link> findLinksByName(String name);

    /**
     * 找到链接,链接id
     *
     * @param linkId 链接id
     * @return {@link Link}
     */
    Link findLinkByLinkId(Integer linkId);
}
