package com.zj.marriage_network.service;

import com.zj.marriage_network.entity.Share;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/5 23:32
 * @Decsription: com.zj.marriage_network.service
 */
public interface ShareService {
    /**
     * 所有条数
     *
     * @return int
     */
    int allCount();
    /**
     * 找到所有分享(已发布)
     *
     * @return {@link List}<{@link Share}>
     */
    List<Share> findAllShare();

    /**
     * 通过状态找到所有分享
     *
     * @param status 状态
     * @return {@link List}<{@link Share}>
     */
    List<Share> findAllShareByStatus(Integer status);

    /**
     * 通过用户id和分享状态找到所有分享
     *
     * @param userId 用户id
     * @param status 状态
     * @return {@link List}<{@link Share}>
     */
    List<Share> findAllShareByUserId(Integer userId,Integer status);

    /**
     * 找到共享通过共享id
     *
     * @param shareId 共享id
     * @return {@link Share}
     */
    Share findShareByShareId(Integer shareId);

    /**
     * 添加分享
     *
     * @param share 分享
     * @return int
     */
    int addShare(Share share);

    /**
     * 更新共享
     *
     * @param share 分享
     * @return int
     */
    int updateShare(Share share);

    /**
     * 更新共享状态  通过，不通过，删除
     *
     * @param share 分享
     * @return int
     */
    int updateShareStatus(Share share);

    /**
     * 更新共享被收藏量
     *
     * @param share 分享
     * @return int
     */
    int updateShareLike(Share share);
}
