package com.zj.marriage_network.service;

import com.zj.marriage_network.entity.ExportList;
import com.zj.marriage_network.entity.Sign;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/7 23:52
 * @Decsription: com.zj.marriage_network.controller
 */
public interface SignService {

    /**
     * 通过活动id找到所有
     *
     * @param activityId 活动id
     * @return {@link List}<{@link Sign}>
     */
    List<Sign> findAllByActivityId(Integer activityId);

    /**
     * 找到所有活动id excel
     *
     * @param activityId 活动id
     * @return {@link List}<{@link Sign}>
     */
    List<Sign> findAllByActivityIdExcel(Integer activityId);

    /**
     * 找到所有活动id excel excel
     * 找到所有活动id excel
     *
     * @param activityId 活动id
     * @return {@link List}<{@link ExportList}>
     */
    List<ExportList> findAllByActivityIdExcelByExcel(Integer activityId);

    /**
     * 通过用户id找到所有
     *
     * @param userId 用户id
     * @return {@link List}<{@link Sign}>
     */
    List<Sign> findAllByUserId(Integer userId);

    /**
     * 报名数量
     *
     * @param activityId 活动id
     * @return int
     */
    int signNumber(Integer activityId);

    /**
     * 添加报名信息
     *
     * @param sign 标志
     * @return int
     */
    int addSign(Sign sign);
}
