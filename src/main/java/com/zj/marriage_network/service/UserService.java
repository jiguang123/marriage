package com.zj.marriage_network.service;


import com.zj.marriage_network.entity.UserInfo;

import java.util.List;

/**
* @author lx
* @description 针对表【mn_user】的数据库操作Service
* @createDate 2022-09-04 15:23:09
*/
public interface UserService  {
    /**
     * 找到所有用户
     *
     * @return {@link List}<{@link UserInfo}>
     */
    List<UserInfo> findAllUsers();

    /**
     * 找到用户名字
     *
     * @param name 名字
     * @return {@link List}<{@link UserInfo}>
     */
    List<UserInfo> findUsersByName(String name);

    /**
     * 所有用户数量
     *
     * @return int
     */
    int allUsersCount();

    /**
     * 更新用户状态
     *
     * @param user 用户
     * @return int
     */
    int updateUserStatus(UserInfo user);

    /*用户操作*/

    /**
     * 找到用户名和密码
     *
     * @param name     名字
     * @param password 密码
     * @return {@link UserInfo}
     */
    UserInfo findUserByNameAndPassword(String name, String password);
    /**
     * 找到用户名字和电话或电子邮件
     *
     * @param user 用户
     * @return {@link UserInfo}
     */
    UserInfo findUserByNameAndPhoneOrEmail(UserInfo user);
    /**
     * 找到用户用户id
     *
     * @param userId 用户id
     * @return {@link UserInfo}
     */
    UserInfo findUserByUserId(Integer userId);
    /**
     * 用户登录名找到用户
     *
     * @param loginName 登录名
     * @return {@link UserInfo}
     */
    UserInfo findUserByLoginName(String loginName);
    /**
     * 添加用户
     *
     * @param user 用户
     * @return int
     */
    int addUser(UserInfo user);

    /**
     * 更新用户
     *
     * @param user 用户
     * @return int
     */
    int updateUser(UserInfo user);

    /**
     * 删除用户
     *
     * @param user 用户
     * @return int
     */
    int removeUser(UserInfo user);

    /**
     * 更新用户密码
     *
     * @param user 用户
     * @return int
     */
    int updateUserPassword(UserInfo user);

    /**
     * 更新用户被收藏数量
     *
     * @param user 用户
     * @return int
     */
    int updateUserNumber(UserInfo user);

    /**
     * 找到可用用户 当status=0
     *
     * @return {@link List}<{@link UserInfo}>
     */
    List<UserInfo> findUserOnline(Integer userId);
    /**
     * 找到用户,用户性别或用户星座
     *
     * @param userInfo 用户信息
     * @return {@link List}<{@link UserInfo}>
     */
    List<UserInfo> findUserByUserSexOrUserConstellation(UserInfo userInfo);
}
