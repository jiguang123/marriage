package com.zj.marriage_network.service.impl;

import com.zj.marriage_network.entity.Activity;
import com.zj.marriage_network.mapper.ActivityMapper;
import com.zj.marriage_network.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/5 23:41
 * @Decsription: com.zj.marriage_network.service.impl
 */
@Service
public class ActivityServiceImpl implements ActivityService {
    @Autowired
    private ActivityMapper activityMapper;
    /**
     * 所有条数
     *
     * @return int
     */
    @Override
    public int allCount() {
        return activityMapper.allCount();
    }

    /**
     * 找到所有活动
     *
     * @return {@link List}<{@link Activity}>
     */
    @Override
    public List<Activity> findAllActivity() {
        return activityMapper.findAllActivity();
    }

    /**
     * 找到活动id
     *
     * @param activityId 活动id
     * @return {@link List}<{@link Activity}>
     */
    @Override
    public Activity findActivityById(Integer activityId) {
        return activityMapper.findActivityById(activityId);
    }

    /**
     * 找到活动状态
     *
     * @param activityStatus 活动状态
     * @return {@link List}<{@link Activity}>
     */
    @Override
    public List<Activity> findActivityByStatus(Integer activityStatus) {
        return activityMapper.findActivityByStatus(activityStatus);
    }

    /**
     * 添加活动
     *
     * @param activity 活动
     * @return int
     */
    @Override
    public int addActivity(Activity activity) {
        return activityMapper.addActivity(activity);
    }

    /**
     * 删除活动
     *
     * @param activity 活动
     * @return int
     */
    @Override
    public int removeActivity(Activity activity) {
        return activityMapper.removeActivity(activity);
    }

    /**
     * 更新活动
     *
     * @param activity 活动
     * @return int
     */
    @Override
    public int updateActivity(Activity activity) {
        return activityMapper.updateActivity(activity);
    }

    /**
     * 更新活动状态
     *
     * @param activity 活动
     * @return int
     */
    @Override
    public int updateActivityStatus(Activity activity) {
        return activityMapper.updateActivityStatus(activity);
    }

    /**
     * 更新活动的被收藏量
     *
     * @param activity 活动
     * @return int
     */
    @Override
    public int updateActivityLike(Activity activity) {
        return activityMapper.updateActivityLike(activity);
    }

    /**
     * 更新活动空余量
     *
     * @param activity 活动
     * @return int
     */
    @Override
    public int updateActivitySpare(Activity activity) {
        return activityMapper.updateActivitySpare(activity);
    }
}
