package com.zj.marriage_network.service.impl;

import com.zj.marriage_network.entity.Admin;
import com.zj.marriage_network.mapper.AdminMapper;
import com.zj.marriage_network.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: ZJ
 * @Date: 2022/9/5 9:39
 * @Decsription: com.zj.marriage_network.service.impl
 */
@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    AdminMapper adminMapper;
    /**
     * 找到管理通过id
     *
     * @param id id
     * @return {@link Admin}
     */
    @Override
    public Admin findAdminById(Integer id) {
        return adminMapper.findAdminById(id);
    }


    /**
     * 找到管理名字和密码
     *
     * @param adminName     管理员名称
     * @param adminPassword 管理员密码
     * @return {@link Admin}
     */
    @Override
    public Admin findAdminByNameAndPassword(String adminName, String adminPassword) {
        return adminMapper.findAdminByNameAndPassword(adminName, adminPassword);
    }

    /**
     * 找到管理名字和邮件
     *
     * @param adminName 管理员名称
     * @param adminMail 管理邮件
     * @return {@link Admin}
     */
    @Override
    public Admin findAdminByNameAndMail(String adminName, String adminMail) {
        return adminMapper.findAdminByNameAndMail(adminName, adminMail);
    }

    /**
     * 更新管理通过id
     *
     * @param admin 管理
     * @return int
     */
    @Override
    public int updateAdminById(Admin admin) {
        return adminMapper.updateAdminById(admin);
    }

    /**
     * 更新管理密码通过id
     *
     * @param admin 管理
     * @return int
     */
    @Override
    public int updateAdminPasswordById(Admin admin) {
        return adminMapper.updateAdminPasswordById(admin);
    }
}
