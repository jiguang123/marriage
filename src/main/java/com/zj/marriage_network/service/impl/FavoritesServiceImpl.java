package com.zj.marriage_network.service.impl;

import com.zj.marriage_network.entity.Favorites;
import com.zj.marriage_network.mapper.FavoritesMapper;
import com.zj.marriage_network.service.FavoritesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/9 16:18
 * @Decsription: com.zj.marriage_network.service.impl
 */
@Service
public class FavoritesServiceImpl implements FavoritesService {
    @Autowired
    FavoritesMapper favoritesMapper;
    /**
     * 添加收藏夹
     *
     * @param favorites 最喜欢
     * @return int
     */
    @Override
    public int addFavorites(Favorites favorites) {
        return favoritesMapper.addFavorites(favorites);
    }

    /**
     * 删除收藏夹
     *
     * @param favorites 最喜欢
     * @return int
     */
    @Override
    public int removeFavorites(Favorites favorites) {
        return favoritesMapper.removeFavorites(favorites);
    }

    /**
     * 找到所有收藏
     *
     * @return {@link List}<{@link Favorites}>
     */
    @Override
    public List<Favorites> getFavorites() {
        return favoritesMapper.getFavorites();
    }

    /**
     * 用户id和其他id找到收藏
     *
     * @param favorites 最喜欢
     * @return {@link Favorites}
     */
    @Override
    public Favorites findByUserIdAndOtherId(Favorites favorites) {
        return favoritesMapper.findByUserIdAndOtherId(favorites);
    }
}
