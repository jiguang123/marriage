package com.zj.marriage_network.service.impl;

import com.zj.marriage_network.entity.Feedback;
import com.zj.marriage_network.mapper.FeedbackMapper;
import com.zj.marriage_network.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/5 23:55
 * @Decsription: com.zj.marriage_network.service.impl
 */
@Service
public class FeedbackServiceImpl implements FeedbackService {
    @Autowired
    private FeedbackMapper feedbackMapper;
    /**
     * 所有条数
     *
     * @return int
     */
    @Override
    public int allCount() {
        return feedbackMapper.allCount();
    }

    /**
     * 添加反馈
     *
     * @param feedback 反馈
     * @return int
     */
    @Override
    public int addFeedback(Feedback feedback) {
        return feedbackMapper.addFeedback(feedback);
    }

    /**
     * 更新反馈状态
     *
     * @param feedback 反馈
     * @return int
     */
    @Override
    public int updateFeedbackStatus(Feedback feedback) {
        return feedbackMapper.updateFeedbackStatus(feedback);
    }

    /**
     * 发现通过反馈id
     *
     * @param feedbackId 反馈id
     * @return {@link Feedback}
     */
    @Override
    public Feedback findByFeedbackId(Integer feedbackId) {
        return feedbackMapper.findByFeedbackId(feedbackId);
    }

    /**
     * 得到反馈列表
     *
     * @return {@link List}<{@link Feedback}>
     */
    @Override
    public List<Feedback> getFeedbackList() {
        return feedbackMapper.getFeedbackList();
    }

    /**
     * 通过状态找到反馈
     *
     * @param feedbackStatus feedback status
     * @return {@link List}<{@link Feedback}>
     */
    @Override
    public List<Feedback> findFeedbackByStatus(Integer feedbackStatus) {
        return feedbackMapper.findFeedbackByStatus(feedbackStatus);
    }
}
