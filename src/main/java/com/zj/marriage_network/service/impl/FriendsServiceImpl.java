package com.zj.marriage_network.service.impl;

import com.zj.marriage_network.entity.Friend;
import com.zj.marriage_network.mapper.FriendsMapper;
import com.zj.marriage_network.service.FriendsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/10 10:38
 * @Decsription: com.zj.marriage_network.service.impl
 */
@Service
public class FriendsServiceImpl implements FriendsService {
    @Autowired
    private FriendsMapper friendsMapper;


    /**
     * 找到朋友状态
     *
     * @param myId   我身份证
     * @param yourId 你身份证
     * @return {@link Friend}
     */
    @Override
    public Friend findFriendStatus(Integer myId, Integer yourId) {
        return friendsMapper.findFriendStatus(myId, yourId);
    }

    /**
     * 查看是否已经申请过好友
     *
     * @param myId   我身份证
     * @param yourId 你身份证
     * @return {@link Friend}
     */
    @Override
    public Friend findFriendStatusWhenOne(Integer myId, Integer yourId) {
        return friendsMapper.findFriendStatusWhenOne(myId, yourId);
    }

    /**
     * 发现我所有朋友根据状态
     *
     * @param userId 用户id
     * @return {@link List}<{@link Friend}>
     */
    @Override
    public List<Friend> findAllFriendMyByStatus(Integer userId) {
        return friendsMapper.findAllFriendMyByStatus(userId);
    }

    /**
     * 找到我所有朋友申请
     *
     * @param userId 用户id
     * @return {@link List}<{@link Friend}>
     */
    @Override
    public List<Friend> findMyAllFriendByStatusApply(Integer userId) {
        return friendsMapper.findMyAllFriendByStatusApply(userId);
    }

    /**
     * 更新朋友状态
     *
     * @param friend 朋友
     * @return int
     */
    @Override
    public int updateFriendStatus(Friend friend) {
        return friendsMapper.updateFriendStatus(friend);
    }

    /**
     * 添加朋友
     *
     * @param myId   我身份证
     * @param yourId 你身份证
     * @return int
     */
    @Override
    public int addFriend(Integer myId, Integer yourId) {
        return friendsMapper.addFriend(myId, yourId);
    }
}
