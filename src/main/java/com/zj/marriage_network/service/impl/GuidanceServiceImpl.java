package com.zj.marriage_network.service.impl;

import com.zj.marriage_network.entity.Guidance;
import com.zj.marriage_network.mapper.GuidanceMapper;
import com.zj.marriage_network.service.GuidanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/5 23:52
 * @Decsription: com.zj.marriage_network.service.impl
 */
@Service
public class GuidanceServiceImpl implements GuidanceService {
    @Autowired
    private GuidanceMapper guidanceMapper;
    /**
     * 所有条数
     *
     * @return int
     */
    @Override
    public int allCount() {
        return guidanceMapper.allCount();
    }

    /**
     * 添加指导
     *
     * @param guidance 指导
     * @return int
     */
    @Override
    public int addGuidance(Guidance guidance) {
        return guidanceMapper.addGuidance(guidance);
    }

    /**
     * 更新指导
     *
     * @param guidance 指导
     * @return int
     */
    @Override
    public int updateGuidance(Guidance guidance) {
        return guidanceMapper.updateGuidance(guidance);
    }

    /**
     * 删除指导
     *
     * @param guidanceId 指导id
     * @return int
     */
    @Override
    public int deleteGuidance(Integer guidanceId) {
        return guidanceMapper.deleteGuidance(guidanceId);
    }

    /**
     * 更新指导数量
     *
     * @param guidance 指导
     * @return int
     */
    @Override
    public int updateGuidanceNumber(Guidance guidance) {
        return guidanceMapper.updateGuidanceNumber(guidance);
    }


    /**
     * 找到所有指导
     *
     * @return {@link List}<{@link Guidance}>
     */
    @Override
    public List<Guidance> findAllGuidance() {
        return guidanceMapper.findAllGuidance();
    }

    /**
     * 找到所有指导 模糊查询
     *
     * @param guidance 指导
     * @return {@link List}<{@link Guidance}>
     */
    @Override
    public List<Guidance> findAllGuidanceByLike(Guidance guidance) {
        return guidanceMapper.findAllGuidanceByLike(guidance);
    }

    /**
     * 通过id获取指导
     *
     * @param guidanceId 指导id
     * @return {@link Guidance}
     */
    @Override
    public Guidance getGuidanceById(Integer guidanceId) {
        return guidanceMapper.getGuidanceById(guidanceId);
    }
}
