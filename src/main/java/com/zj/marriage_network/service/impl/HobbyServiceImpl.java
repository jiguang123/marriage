package com.zj.marriage_network.service.impl;

import com.zj.marriage_network.entity.Hobby;
import com.zj.marriage_network.mapper.HobbyMapper;
import com.zj.marriage_network.service.HobbyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: ZJ
 * @Date: 2022/9/10 14:50
 * @Decsription: com.zj.marriage_network.service.impl
 */
@Service
public class HobbyServiceImpl implements HobbyService {
    @Autowired
    HobbyMapper hobbyMapper;
    /**
     * 通过用户id找到喜好
     *
     * @param userId 用户id
     * @return {@link Hobby}
     */
    @Override
    public Hobby findByUserId(Integer userId) {
        return hobbyMapper.findByUserId(userId);
    }

    /**
     * 更新爱好
     *
     * @param hobby 爱好
     * @return int
     */
    @Override
    public int updateHobby(Hobby hobby) {
        return hobbyMapper.updateHobby(hobby);
    }

    /**
     * 添加爱好
     *
     * @param hobby 爱好
     * @return int
     */
    @Override
    public int addHobby(Hobby hobby) {
        return hobbyMapper.addHobby(hobby);
    }
}
