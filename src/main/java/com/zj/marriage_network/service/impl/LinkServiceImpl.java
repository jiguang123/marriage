package com.zj.marriage_network.service.impl;

import com.zj.marriage_network.entity.Link;
import com.zj.marriage_network.mapper.LinkMapper;
import com.zj.marriage_network.service.LinkService;
import org.junit.AfterClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/5 23:54
 * @Decsription: com.zj.marriage_network.service.impl
 */
@Service
public class LinkServiceImpl implements LinkService {
    @Autowired
    private LinkMapper linkMapper;
    /**
     * 所有条数
     *
     * @return int
     */
    @Override
    public int allCount() {
        return linkMapper.allCount();
    }

    /**
     * 添加链接
     *
     * @param link 链接
     * @return int
     */
    @Override
    public int addLink(Link link) {
        return linkMapper.addLink(link);
    }

    /**
     * 更新链接
     *
     * @param link 链接
     * @return int
     */
    @Override
    public int updateLink(Link link) {
        return linkMapper.updateLink(link);
    }

    /**
     * 删除链接
     *
     * @param link 链接
     * @return int
     */
    @Override
    public int deleteLink(Link link) {
        return linkMapper.deleteLink(link);
    }

    /**
     * 找到所有链接
     *
     * @return {@link List}<{@link Link}>
     */
    @Override
    public List<Link> findAllLinks() {
        return linkMapper.findAllLinks();
    }

    /**
     * 发现链接名字
     *
     * @param name 名字
     * @return {@link List}<{@link Link}>
     */
    @Override
    public List<Link> findLinksByName(String name) {
        return linkMapper.findLinksByName(name);
    }

    /**
     * 找到链接,链接id
     *
     * @param linkId 链接id
     * @return {@link Link}
     */
    @Override
    public Link findLinkByLinkId(Integer linkId) {
        return linkMapper.findLinkByLinkId(linkId);
    }
}
