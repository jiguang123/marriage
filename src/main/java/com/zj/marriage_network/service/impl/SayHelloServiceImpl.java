package com.zj.marriage_network.service.impl;

import com.zj.marriage_network.entity.SayHello;
import com.zj.marriage_network.mapper.SayHelloMapper;
import com.zj.marriage_network.service.SayHelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/10 12:48
 * @Decsription: com.zj.marriage_network.service.impl
 */
@Service
public class SayHelloServiceImpl implements SayHelloService {
    @Autowired
    SayHelloMapper sayHelloMapper;
    /**
     * 找到用户id和其他id
     *
     * @param userId  用户id
     * @param otherId 其他id
     * @return {@link SayHello}
     */
    @Override
    public SayHello findByUserIdAndOtherId(Integer userId, Integer otherId) {
        return sayHelloMapper.findByUserIdAndOtherId(userId, otherId);
    }

    /**
     * 添加问好
     *
     * @param userId  用户id
     * @param otherId 其他id
     * @return int
     */
    @Override
    public int addSayHello(Integer userId, Integer otherId) {
        return sayHelloMapper.addSayHello(userId, otherId);
    }

    /**
     * 找到所有给我的打招呼
     *
     * @return {@link List}<{@link SayHello}>
     */
    @Override
    public List<SayHello> findAllMySayHello(Integer userId) {
        return sayHelloMapper.findAllMySayHello(userId);
    }

    /**
     * 找到所有给我的打招呼通过状态
     *
     * @param sayHello 说“你好”
     * @return {@link List}<{@link SayHello}>
     */
    @Override
    public List<SayHello> findAllMySayHelloByStatus(SayHello sayHello) {
        return sayHelloMapper.findAllMySayHelloByStatus(sayHello);
    }

    /**
     * 更新打招呼的状态
     *
     * @param sayHello 说“你好”
     * @return int
     */
    @Override
    public int updateHelloStatus(SayHello sayHello) {
        return sayHelloMapper.updateHelloStatus(sayHello);
    }

    /**
     * 修改我的所有打招呼的信息
     *
     * @param sayHello 说“你好”
     * @return int
     */
    @Override
    public int updateHelloStatusByMy(SayHello sayHello) {
        return sayHelloMapper.updateHelloStatusByMy(sayHello);
    }

    /**
     * 删除打招呼
     *
     * @param sayId 说id
     * @return int
     */
    @Override
    public int deleteSayHello(Integer sayId) {
        return sayHelloMapper.deleteSayHello(sayId);
    }

    /**
     * 我没有看过的消息的数量
     *
     * @param userId 用户id
     * @return int
     */
    @Override
    public int mySayHelloNoLook(Integer userId) {
        return sayHelloMapper.mySayHelloNoLook(userId);
    }
}
