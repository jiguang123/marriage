package com.zj.marriage_network.service.impl;

import com.zj.marriage_network.entity.Share;
import com.zj.marriage_network.mapper.ShareMapper;
import com.zj.marriage_network.service.ShareService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/5 23:33
 * @Decsription: com.zj.marriage_network.service.impl
 */
@Service
public class ShareServiceImpl implements ShareService {
    @Autowired
    private ShareMapper shareMapper;
    /**
     * 所有条数
     *
     * @return int
     */
    @Override
    public int allCount() {
        return shareMapper.allCount();
    }

    /**
     * 找到所有分享
     *
     * @return {@link List}<{@link Share}>
     */
    @Override
    public List<Share> findAllShare() {
        return shareMapper.findAllShare();
    }

    /**
     * 通过状态找到所有分享
     *
     * @param status 状态
     * @return {@link List}<{@link Share}>
     */
    @Override
    public List<Share> findAllShareByStatus(Integer status) {
        return shareMapper.findAllShareByStatus(status);
    }

    /**
     * 通过用户id和分享状态找到所有分享
     *
     * @param userId 用户id
     * @param status 状态
     * @return {@link List}<{@link Share}>
     */
    @Override
    public List<Share> findAllShareByUserId(Integer userId, Integer status) {
        return shareMapper.findAllShareByUserId(userId, status);
    }

    /**
     * 找到共享通过共享id
     *
     * @param shareId 共享id
     * @return {@link Share}
     */
    @Override
    public Share findShareByShareId(Integer shareId) {
        return shareMapper.findShareByShareId(shareId);
    }

    /**
     * 添加分享
     *
     * @param share 分享
     * @return int
     */
    @Override
    public int addShare(Share share) {
        return shareMapper.addShare(share);
    }

    /**
     * 更新共享
     *
     * @param share 分享
     * @return int
     */
    @Override
    public int updateShare(Share share) {
        return shareMapper.updateShare(share);
    }

    /**
     * 更新共享状态  通过，不通过，删除
     *
     * @param share 分享
     * @return int
     */
    @Override
    public int updateShareStatus(Share share) {
        return shareMapper.updateShareStatus(share);
    }

    /**
     * 更新共享被收藏量
     *
     * @param share 分享
     * @return int
     */
    @Override
    public int updateShareLike(Share share) {
        return shareMapper.updateShareLike(share);
    }
}
