package com.zj.marriage_network.service.impl;

import com.zj.marriage_network.entity.ExportList;
import com.zj.marriage_network.entity.Sign;
import com.zj.marriage_network.mapper.SignMapper;
import com.zj.marriage_network.service.SignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: ZJ
 * @Date: 2022/9/7 23:56
 * @Decsription: com.zj.marriage_network.service.impl
 */
@Service
public class SignServiceImpl implements SignService {
    @Autowired
    SignMapper signMapper;
    @Override
    public List<Sign> findAllByActivityId(Integer activityId) {
        return signMapper.findAllByActivityId(activityId);
    }

    /**
     * 找到所有活动id excel
     *
     * @param activityId 活动id
     * @return {@link List}<{@link Sign}>
     */
    @Override
    public List<Sign> findAllByActivityIdExcel(Integer activityId) {
        return signMapper.findAllByActivityIdExcel(activityId);
    }

    /**
     * 找到所有活动id excel excel
     * 找到所有活动id excel
     *
     * @param activityId 活动id
     * @return {@link List}<{@link ExportList}>
     */
    @Override
    public List<ExportList> findAllByActivityIdExcelByExcel(Integer activityId) {
        return signMapper.findAllByActivityIdExcelByExcel(activityId);
    }

    /**
     * 通过用户id找到所有
     *
     * @param userId 用户id
     * @return {@link List}<{@link Sign}>
     */
    @Override
    public List<Sign> findAllByUserId(Integer userId) {
        return signMapper.findAllByUserId(userId);
    }

    /**
     * 报名数量
     *
     * @param activityId 活动id
     * @return int
     */
    @Override
    public int signNumber(Integer activityId) {
        return signMapper.signNumber(activityId);
    }

    /**
     * 添加报名信息
     *
     * @param sign 标志
     * @return int
     */
    @Override
    public int addSign(Sign sign) {
        return signMapper.addSign(sign);
    }
}
