package com.zj.marriage_network.service.impl;


import com.zj.marriage_network.entity.UserInfo;
import com.zj.marriage_network.mapper.UserMapper;
import com.zj.marriage_network.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author lx
* @description 针对表【mn_user】的数据库操作Service实现
* @createDate 2022-09-04 15:23:09
*/
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    /**
     * 找到所有用户
     *
     * @return {@link List}<{@link UserInfo}>
     */
    @Override
    public List<UserInfo> findAllUsers() {

        return userMapper.findAllUsers();
    }

    /**
     * 找到用户名字
     *
     * @param name 名字
     * @return {@link List}<{@link UserInfo}>
     */
    @Override
    public List<UserInfo> findUsersByName(String name) {
        return userMapper.findUsersByName(name);
    }

    /**
     * 所有用户数量
     *
     * @return int
     */
    @Override
    public int allUsersCount() {
        return userMapper.allUsersCount();
    }

    /**
     * 更新用户状态
     *
     * @param user 用户
     * @return int
     */
    @Override
    public int updateUserStatus(UserInfo user) {
        return userMapper.updateUserStatus(user);
    }

    /**
     * 找到用户名和密码
     *
     * @param name     名字
     * @param password 密码
     * @return {@link UserInfo}
     */
    @Override
    public UserInfo findUserByNameAndPassword(String name, String password) {
        return userMapper.findUserByNameAndPassword(name, password);
    }

    /**
     * 找到用户名字和电话或电子邮件
     *
     * @param user 用户
     * @return {@link UserInfo}
     */
    @Override
    public UserInfo findUserByNameAndPhoneOrEmail(UserInfo user) {
        return userMapper.findUserByNameAndPhoneOrEmail(user);
    }

    /**
     * 找到用户用户id
     *
     * @param userId 用户id
     * @return {@link UserInfo}
     */
    @Override
    public UserInfo findUserByUserId(Integer userId) {
        return userMapper.findUserByUserId(userId);
    }

    /**
     * 用户登录名找到用户
     *
     * @param loginName 登录名
     * @return {@link UserInfo}
     */
    @Override
    public UserInfo findUserByLoginName(String loginName) {
        return userMapper.findUserByLoginName(loginName);
    }

    /**
     * 添加用户
     *
     * @param user 用户
     * @return int
     */
    @Override
    public int addUser(UserInfo user) {
        return userMapper.addUser(user);
    }

    /**
     * 更新用户
     *
     * @param user 用户
     * @return int
     */
    @Override
    public int updateUser(UserInfo user) {
        return userMapper.updateUser(user);
    }

    /**
     * 删除用户
     *
     * @param user 用户
     * @return int
     */
    @Override
    public int removeUser(UserInfo user) {
        return userMapper.removeUser(user);
    }

    /**
     * 更新用户密码
     *
     * @param user 用户
     * @return int
     */
    @Override
    public int updateUserPassword(UserInfo user) {
        return userMapper.updateUserPassword(user);
    }

    /**
     * 更新用户被收藏数量
     *
     * @param user 用户
     * @return int
     */
    @Override
    public int updateUserNumber(UserInfo user) {
        return userMapper.updateUserNumber(user);
    }

    /**
     * 找到可用用户 当status=0
     *
     * @return {@link List}<{@link UserInfo}>
     */
    @Override
    public List<UserInfo> findUserOnline(Integer userId) {
        return userMapper.findUserOnline(userId);
    }

    /**
     * 找到用户,用户性别或用户星座
     *
     * @param userInfo 用户信息
     * @return {@link List}<{@link UserInfo}>
     */
    @Override
    public List<UserInfo> findUserByUserSexOrUserConstellation(UserInfo userInfo) {
        return userMapper.findUserByUserSexOrUserConstellation(userInfo);
    }
}
