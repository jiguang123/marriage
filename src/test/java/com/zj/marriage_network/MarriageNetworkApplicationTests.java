package com.zj.marriage_network;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.write.builder.ExcelWriterBuilder;
import com.zj.marriage_network.entity.*;
import com.zj.marriage_network.mapper.ActivityMapper;
import com.zj.marriage_network.mapper.ShareMapper;
import com.zj.marriage_network.mapper.SignMapper;
import com.zj.marriage_network.service.FeedbackService;
import com.zj.marriage_network.service.FriendsService;
import org.junit.AfterClass;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootTest
class MarriageNetworkApplicationTests {


    

    @Test
    void contextLoads() {
        java.sql.Timestamp date= new java.sql.Timestamp(System.currentTimeMillis());
        System.out.println(date);
    }
    @Autowired
    FeedbackService feedbackService;
    @Test
    void getFeedbackList() {
        System.out.println(feedbackService.getFeedbackList());
    }

    @Autowired
    ShareMapper shareMapper;
    @Autowired
    SignMapper signMapper;

    @Test
    void getShareList() {
        SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        List<Share> shareList=shareMapper.findAllShare();
        System.out.println();
        List<Sign> signList=signMapper.findAllByActivityId(1);
        System.out.println(signList);
    }

    @Autowired
    ActivityMapper activityMapper;
    @Test
    void getSignList() {
        List<ExportList> exportList=signMapper.findAllByActivityIdExcelByExcel(1);
        Activity activity= activityMapper.findActivityById(1);
        System.out.println(exportList);
        String filename="E:\\"+activity.getActivityTitle()+"报名信息表.xlsx";
        EasyExcel.write(filename, ExportList.class).sheet(activity.getActivityTitle()+"报名信息表.xlsx").doWrite(exportList);

    }

    /*public void goToFirm(HttpServletResponse response) throws IOException {
        try {
            List<ExportList> exportList=signMapper.findAllByActivityIdExcelByExcel(1);
            Activity activity= activityMapper.findActivityById(1);
            String fileName = new String("报名信息表.xlsx".getBytes(), "ISO-8859-1");
            response.addHeader("Content-Disposition", "filename=" + fileName);
            ServletOutputStream out = response.getOutputStream();
            EasyExcelFactory.write(out,ExportList.class).sheet(activity.getActivityTitle()+"报名信息表.xlsx").doWrite(exportList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }*/

    @Autowired
    FriendsService friendsService;
    @Test
    public void findMyAllFriendByStatusApplyTest(){
        List<Friend> friendList = friendsService.findMyAllFriendByStatusApply(1);
        System.out.println(friendList);
    }
}
